# Sources de données

Les données présentes dans ce dossier sont mises à disposition par la Direction Générale des Collectivités Locales (DGCL). 

Les données utilisées dans ce projet ne concernent que les communes. 

Deux types de fichiers de données sont présents : 
* Les fichiers des montants des différentes dotations accordés à chaque commune par année
* Le fichier des critères de répartition utilisés pour le calcul des dotations. Ce fichier comprend aussi les montants des dotations

Les données ont été converties au format csv pour faciliter leur manipulation dans le projet. 

### Téléchargement 

Les données sont accessibles et téléchargeables via le [site](http://www.dotations-dgcl.interieur.gouv.fr/consultation/accueil.php) de la DGCL. 

Pour les dotations, la DGCL met à disposition un fichier par type de dotations et par année. 
Ils sont téléchargeables via cette [page](http://www.dotations-dgcl.interieur.gouv.fr/consultation/dotations_en_ligne.php) en choississant l'année et le type de dotation à l'aide des menus déroulants. 

Pour les critères de rérpartitions, la DGCL met à disposition un fichier par année et par type de collectivité (commune, EPCI, département, région). 
Ils sont téléchargeables via cette [page](http://www.dotations-dgcl.interieur.gouv.fr/consultation/criteres_repartition.php) en choississant l'année et le type de dotation à l'aide des menus déroulants. 