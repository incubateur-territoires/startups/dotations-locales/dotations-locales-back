# Usage example for 5 timing per function and where python warnings are ignored:
# python -W ignore tests/check_performance.py -v -c 5

import argparse
import logging
import sys
import time

from collections import defaultdict
from statistics import mean

from dotations_locales_back.simulation.simulation import (
    CURRENT_YEAR,
    data_2021_as_last_year,
    fully_adapted_data_2022,
    simulation_from_dgcl_csv,
    tbs
)


logger = logging.getLogger(__name__)
performances = defaultdict(list)

def measure_time(method):
    def timed(*args, **kwargs):
        start_time = time.time()
        result = method(*args, **kwargs)
        duration = time.time() - start_time
        logger.info('⏰ {:2.6f} s'.format(duration))

        subkey = '' if args[1:] == ()  else args[1:][0]  # openfisca variable name
        performances[method.__name__+'-'+subkey].append(duration)

        return result

    return timed


@measure_time
def load_simulation():
    load_simulation_silently()


def load_simulation_silently():
    # default: load simulation without measuring time
    simulation_2022 = simulation_from_dgcl_csv(
        CURRENT_YEAR,
        fully_adapted_data_2022,
        tbs,
        data_previous_year=data_2021_as_last_year,
    )
    return simulation_2022


@measure_time
def calculate(simulation, openfisca_variable_name):
    simulation.calculate(openfisca_variable_name, CURRENT_YEAR)


def main():
    parser = argparse.ArgumentParser(description = __doc__)
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = 'increase output verbosity')
    parser.add_argument('-c', '--count', action = 'store', default = 1, help = "iterate count times", type = int)
    
    global args
    args, _ = parser.parse_known_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)
    
    logger.info('⏳ load_simulation')
    for _ in range(args.count):
        load_simulation()

    logger.info('⏳ calculate dotation_forfaitaire')
    for _ in range(args.count):
        # bypass simulation cache by calculating on a new one
        simulation_2022 = load_simulation_silently()
        calculate(simulation_2022, 'dotation_forfaitaire')
    
    logger.info('⏳ calculate dotation_solidarite_rurale')
    for _ in range(args.count):
        # bypass simulation cache by calculating on a new one
        simulation_2022 = load_simulation_silently()
        calculate(simulation_2022, 'dotation_solidarite_rurale')

    logger.info('⏳ calculate dsu_montant')
    for _ in range(args.count):
        # bypass simulation cache by calculating on a new one
        simulation_2022 = load_simulation_silently()
        calculate(simulation_2022, 'dsu_montant')

    logger.info('📚 moyenne')
    for k in performances.keys():
        print(k, '{:2.6f} s'.format(mean(performances[k])))


if __name__ == '__main__':
    sys.exit(main())
