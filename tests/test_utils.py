from os.path import sep

from dotations_locales_back.common import EchelonTerritorial
from dotations_locales_back.common.utils import get_territory_past_filiation


def test_get_territory_past_filiation_commune():
    annee_reference = 2023
    annee_cible = 2022
    code_insee_commune = "50272"  # TOURNEVILLE-SUR-MER

    past_filiation = get_territory_past_filiation(
        EchelonTerritorial.COMMUNE, code_insee_commune, annee_reference, annee_cible
    )
    assert past_filiation == ["50015", "50272"]


def test_get_territory_past_filiation_commune__no_change():
    # unchanged between 2022 and 2023
    annee_reference = 2023
    annee_cible = 2022
    code_insee_commune = "71001"  # ABERGEMENT-DE-CUISERY

    past_filiation = get_territory_past_filiation(
        EchelonTerritorial.COMMUNE, code_insee_commune, annee_reference, annee_cible
    )
    assert past_filiation == ["71001"]


def test_get_territory_past_filiation_commune__pluriannual():
    # changed this year
    annee_reference = 2023
    annee_cible = 2021
    code_insee_commune = "50272"  # TOURNEVILLE-SUR-MER, as defined in 2023
    # 50272/TOURNEVILLE-SUR-MER (2023) = 50015/ANNOVILLE + 50272/LINGREVILLE (2022)
    
    past_filiation = get_territory_past_filiation(
        EchelonTerritorial.COMMUNE, code_insee_commune, annee_reference, annee_cible
    )
    assert past_filiation == ["50015", "50272"]


def test_get_territory_past_filiation_commune__pluriannual_2():
    # changed one year ago
    annee_reference = 2022
    annee_cible = 2020
    code_insee_commune = "16233"  # MOSNAC-SAINT-SIMEUX 2022, as defined in 2021
    # 16233/MOSNAC-SAINT-SIMEUX (2021) = 16233/MOSNAC + 16351,SAINT-SIMEUX (2020)

    past_filiation = get_territory_past_filiation(
        EchelonTerritorial.COMMUNE, code_insee_commune, annee_reference, annee_cible
    )
    assert past_filiation == ["16233", "16351"]


def test_get_territory_past_filiation_commune__pluriannual_3():
    # changed two times during period
    annee_reference = 2023
    annee_cible = 2020
    code_insee_commune = "27676"  # LES TROIS LACS, 2023
    # 27676 in 2021, 27058 in 2022 and 27676 in 2023

    past_filiation = get_territory_past_filiation(
        EchelonTerritorial.COMMUNE, code_insee_commune, annee_reference, annee_cible
    )
    assert past_filiation == ["27676"]


def test_get_territory_past_filiation_commune__pluriannual_missing_data():
    annee_reference = 2023
    annee_cible = 2000
    code_insee_commune = "50272"  # TOURNEVILLE-SUR-MER, as defined in 2023
    # 50272/TOURNEVILLE-SUR-MER (2023) = 50015/ANNOVILLE + 50272/LINGREVILLE (2022)

    fnfe = None
    try:
        past_filiation = get_territory_past_filiation(
            EchelonTerritorial.COMMUNE, code_insee_commune, annee_reference, annee_cible
        )
    except Exception as e:
        fnfe = e

    # fail on first year missing data
    message = str(fnfe)
    assert fnfe.__class__ == FileNotFoundError
    
    filename_expected = "filiation_commune_2019-2020.csv"
    filename_error_index = str.rfind(message, sep) + 1
    assert filename_expected in message, f"La recherche de filiation est interrompue par l'absence de '{message[filename_error_index:]} au lieu de l'absence attendue de '{filename_expected}'."


def test_get_territory_past_filiation_epci():
    annee_reference = 2023
    annee_cible = 2022
    code_siren_epci = "200070233"  # CA TERRES DE MONTAIGU, as defined in 2022
    # 200070233/CC TERRES DE MONTAIGU, COMMUNAUTE DE COMMUNES MONTAIGU-ROCHESERVIERE (2021)

    past_filiation = get_territory_past_filiation(
        EchelonTerritorial.EPCI, code_siren_epci, annee_reference, annee_cible
    )
    assert past_filiation == ["200070233"]
