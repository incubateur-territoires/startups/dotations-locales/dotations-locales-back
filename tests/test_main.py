from fastapi.testclient import TestClient

from dotations_locales_back.web_api.main import app
from dotations_locales_back.common.load_dgcl_criteres_data import (
    load_commune_dgcl_data,
    load_epci_dgcl_data,
    load_dpt_dgcl_data,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2023 import (
    filename_commune_criteres_2023,
)
from dotations_locales_back.common.mapping.epci.criteres_dgcl_2023 import (
    filename_epci_criteres_2023,
)
from dotations_locales_back.common.mapping.departement.criteres_dgcl_2023 import (
    filename_departement_criteres_2023,
)

from os import getcwd
import logging

DATA_DIRECTORY = getcwd() + "/data/"

client = TestClient(app)

# COMMUNES


def test_describe_commune():
    """Teste la fonction de description d'une commune."""
    response = client.post("/commune/", json={"code": "59350"})
    assert response.status_code == 200
    assert response.json()["code"] == "59350"


def test_all_communes():
    """Teste la liste de toutes les communes."""
    # On prend en référence le fichier des dotations forfaitaires 2023 pour récupérer la liste des codes INSEE de toutes les communes de France.
    communes_codes = load_commune_dgcl_data(
        DATA_DIRECTORY + filename_commune_criteres_2023, 2023
    )["code"].to_list()
    # communes_codes = commune_dotations_criteres.get("2023")["code_insee"].to_list()
    for code in communes_codes:
        logging.debug(code)
        response = client.post("/commune/", json={"code": code})
        assert response.status_code == 200
        assert response.json()["code"] == code


def test_describe_commune_invalid_json():
    response = client.post("/commune/", json={"something": "something"})
    assert response.status_code == 422


def test_describe_commune_invalid_code_insee():
    response = client.post("/commune/", json={"code": "INVALID_CODE"})
    assert response.status_code == 404
    assert response.json()["detail"] == "Commune non trouvée"


def test_simulation():
    """Teste la fonction de simulation."""
    body = {
        "code": "59150",
        "periode_loi": "2022",
        "data": {
            "population_insee": 1000,
            "potentiel_financier_par_habitant": 10.20,
            "longueur_voirie": 100,
            "zone_de_montagne": True,
            "superficie": 100,
            "population_enfants": 100,
            "residences_secondaires": 10,
            "places_caravanes_avant_majoration": 5,
        },
    }
    response = client.post("/commune/", json={"code": "59350"})
    assert response.status_code == 200
    assert response.json()["code"] == "59350"


def test_simulation_invalid_json():
    response = client.post("/commune/", json={"someting": "something"})
    assert response.status_code == 422


def test_simulation_invalid_code_insee():
    body = {
        "code": "INVALID",
        "periode_loi": "2022",
        "data": {
            "population_insee": 1010010,
            "population_dgf": 1010010,
            "potentiel_financier": 575,
            "potentiel_financier_par_habitant": 575,
            "effort_fiscal": 1.1083,
            "recettes_reelles_fonctionnement": 1000,
        },
    }
    response = client.post("/simulation/", json=body)
    assert response.status_code == 404
    assert response.json()["detail"] == "Commune non trouvée"


# EPCI


def test_describe_epci():
    """Teste la fonction de description d'une EPCI."""
    response = client.post("/epci/", json={"code": "200041960"})
    assert response.status_code == 200
    assert response.json()["code"] == "200041960"


def test_all_epci():
    """Teste la liste de toutes les EPCI."""
    # On prend en référence le fichier des dotations intercommunalité 2023 pour récupérer la liste des codes Siren de tous les epci de France.
    epci_codes = load_epci_dgcl_data(
        DATA_DIRECTORY + filename_epci_criteres_2023, 2023
    )["code"].to_list()
    for code in epci_codes:
        response = client.post("/epci/", json={"code": code})
        assert response.status_code == 200
        assert response.json()["code"] == code


# DEPARTEMENTS


def test_describe_departement():
    """Teste la fonction de description d'un département."""
    response = client.post("/departement/", json={"code": "59"})
    assert response.status_code == 200
    assert response.json()["code"] == "59"


def test_all_departements():
    """Teste la liste de tous les départements."""
    # On prend en référence le fichier des dotations forfaitaires 2023 pour récupérer la liste des numéros de tous les départements de France.
    departements_codes = load_dpt_dgcl_data(
        DATA_DIRECTORY + filename_departement_criteres_2023, 2023
    )["code"].to_list()
    for code in departements_codes:
        response = client.post("/departement/", json={"code": code})
        assert response.status_code == 200
        assert response.json()["code"] == code


# Files


def test_download_file():
    """Teste la fonction de téléchargement de fichier."""
    response = client.get("/files/criteres_repartition_2022.csv")
    assert response.status_code == 200


def test_download_file_invalid_filename():
    """Teste la fonction de téléchargement de fichier."""
    response = client.get("/files/invalid.csv")
    assert response.status_code == 404


# Init


def test_init():
    """Teste la fonction d'initialisation qui renvoie certaines données au front."""
    response = client.get("/init/")
    assert response.status_code == 200
    assert "simulation_periodes" in response.json()
    assert "base_calcul" in response.json()
    assert "sources_donnees" in response.json()
    assert "derniere_maj_donnees" in response.json()


# Comparaison


def test_comparaison_commune():
    """Teste la fonction de comparaison pour les communes"""
    response = client.post("/comparaison/", json={"code": "59350"})
    assert response.status_code == 200
    assert response.json()["epci"]
    assert response.json()["departement"]


def test_comparaison_epci():
    """Teste la fonction de comparaison pour les départements"""
    response = client.post("/comparaison/", json={"code": "200096683"})
    assert response.status_code == 200
    assert response.json()["departement"]


def test_comparaison_departement():
    """Teste la fonction de comparaison pour les départements"""
    response = client.post("/comparaison/", json={"code": "59"})
    assert response.status_code == 200
    assert response.json()["region"]


def test_comparaison_invalid_code():
    """Teste la fonction de comparaison avec un code invalide."""
    response = client.post("/comparaison/", json={"code": "INVALID"})
    assert response.status_code == 404
    assert response.json()["detail"] == "Code non valide"


if __name__ == "__main__":
    test_all_communes()
