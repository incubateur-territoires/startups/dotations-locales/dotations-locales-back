from dotations_locales_back.web_api.main import (
    commune_dotations_criteres,
    epci_dotations_criteres,
    departement_dotations_criteres,
)
from dotations_locales_back.common.init_datas import (
    COMMUNE_CRITERES_2022_PATH,
    COMMUNE_CRITERES_2021_PATH,
    COMMUNE_CRITERES_2020_PATH,
    EPCI_CRITERES_2022_PATH,
    EPCI_CRITERES_2021_PATH,
    DEPARTEMENT_CRITERES_2022_PATH,
    DEPARTEMENT_CRITERES_2021_PATH,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2022 import (
    montants_dotations_2022 as commune_montants_dotations_2022,
    infos_generales_2022 as commune_infos_generales_2022,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2021 import (
    montants_dotations_2021 as commune_montants_dotations_2021,
    infos_generales_2021 as commune_infos_generales_2021,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2020 import (
    montants_dotations_2020 as commune_montants_dotations_2020,
    infos_generales_2020 as commune_infos_generales_2020,
)
from dotations_locales_back.common.mapping.epci.criteres_dgcl_2022 import (
    montants_dotations_2022 as epci_montants_dotations_2022,
    infos_generales_2022 as epci_infos_generales_2022,
)
from dotations_locales_back.common.mapping.epci.criteres_dgcl_2021 import (
    montants_dotations_2021 as epci_montants_dotations_2021,
    infos_generales_2021 as epci_infos_generales_2021,
)
from dotations_locales_back.common.mapping.departement.criteres_dgcl_2022 import (
    montants_dotations_2022 as departement_montants_dotations_2022,
    infos_generales_2022 as departement_infos_generales_2022,
)
from dotations_locales_back.common.mapping.departement.criteres_dgcl_2021 import (
    montants_dotations_2021 as departement_montants_dotations_2021,
    infos_generales_2021 as departement_infos_generales_2021,
)
import pandas as pd
import numpy
import pytest

from fastapi.testclient import TestClient

from dotations_locales_back.web_api.main import app

client = TestClient(app)

test_params = [
    (
        commune_dotations_criteres.get("2022"),
        COMMUNE_CRITERES_2022_PATH,
        commune_montants_dotations_2022,
        commune_infos_generales_2022,
    ),
    (
        commune_dotations_criteres.get("2021"),
        COMMUNE_CRITERES_2021_PATH,
        commune_montants_dotations_2021,
        commune_infos_generales_2021,
    ),
    (
        commune_dotations_criteres.get("2020"),
        COMMUNE_CRITERES_2020_PATH,
        commune_montants_dotations_2020,
        commune_infos_generales_2020,
    ),
    (
        epci_dotations_criteres.get("2022"),
        EPCI_CRITERES_2022_PATH,
        epci_montants_dotations_2022,
        epci_infos_generales_2022,
    ),
    (
        epci_dotations_criteres.get("2021"),
        EPCI_CRITERES_2021_PATH,
        epci_montants_dotations_2021,
        epci_infos_generales_2021,
    ),
    (
        departement_dotations_criteres.get("2022"),
        DEPARTEMENT_CRITERES_2022_PATH,
        departement_montants_dotations_2022,
        departement_infos_generales_2022,
    ),
    (
        departement_dotations_criteres.get("2021"),
        DEPARTEMENT_CRITERES_2021_PATH,
        departement_montants_dotations_2021,
        departement_infos_generales_2022,
    ),
]


@pytest.mark.parametrize(
    "processed_data, source_data_path, montants_dotations, code", test_params
)
def test_compare_processed_source_data(
    processed_data, source_data_path, montants_dotations, code
):
    source_data = pd.read_csv(
        source_data_path,
        dtype={list(code.keys())[0]: str},
    )
    processed_data = processed_data.merge(
        source_data,
        how="left",
        left_on=list(code.values())[0],
        right_on=list(code.keys())[0],
    )

    amounts_columns = list(montants_dotations.keys()) + list(
        montants_dotations.values()
    )

    processed_data = processed_data[amounts_columns]

    for key, value in montants_dotations.items():
        assert processed_data[key].equals(processed_data[value])


# def test_compare_api_source_data():
#     source_data = pd.read_csv(
#         COMMUNE_CRITERES_2022_PATH,
#         dtype={"Informations générales - Code INSEE de la commune": str},
#     )
#     codes = source_data[
#         "Informations générales - Code INSEE de la commune"
#     ].values.tolist()
#     random_codes = numpy.random.choice(codes, 2, replace=False)
#     for code in random_codes:
#         response = client.post("/commune/", json={"code": code})
#         print(response.json()["dotations"]["dotation_forfaitaire"]["annees"][0]["2022"])
#         print(
#             str(
#                 source_data.loc[
#                     source_data["Informations générales - Code INSEE de la commune"]
#                     == code,
#                     "Dotation forfaitaire - Dotation forfaitaire notifiée N",
#                 ].iloc[0]
#             )
#         )
#         assert response.json()["dotations"]["dotation_forfaitaire"]["annees"][0][
#             "2022"
#         ] == str(
#             source_data.loc[
#                 source_data["Informations générales - Code INSEE de la commune"]
#                 == code,
#                 "Dotation forfaitaire - Dotation forfaitaire notifiée N",
#             ].iloc[0]
#         )


if __name__ == "__main__":
    test_compare_processed_source_data()
