from dotations_locales_back.simulation.simulation import (
    data_2021_as_last_year,
    dgcl_data_2021,
    fully_adapted_data_2022,
    calculate_dotations,
)
from dotations_locales_back.simulation.load_dgcl_data import (
    ajoute_appartenance_outre_mer,
    corrige_potentiel_moyen_strate,
)

from dotations_locales_back.common.mapping.commune.criteres_dgcl_2021 import (
    variables_openfisca_presentes_fichier_2021,
)


def test_previous_year_data():
    """On regarde si les colonnes nécessaires à la simulation Openfisca sont bien présentes dans la dataframe que l'on va entrer dans le modèle."""
    mandatory_columns = [
        "Informations générales - Code INSEE de la commune",
        "dsu_montant_eligible",
        "dsr_montant_eligible_fraction_bourg_centre",
        "dsr_montant_eligible_fraction_perequation",
        "dsr_montant_hors_garanties_fraction_cible",
        "population_dgf_majoree",
        "dotation_forfaitaire",
    ]
    assert list(set(list(data_2021_as_last_year)) - set(mandatory_columns)) == []


def test_simulation_data():
    """On regarde si les colonnes nécessaires à la simulation Openfisca sont bien présentes dans la dataframe que l'on va entrer dans le modèle."""
    mandatory_columns = [
        "bureau_centralisateur",
        "chef_lieu_arrondissement",
        "chef_lieu_de_canton",
        "chef_lieu_departement_dans_agglomeration",
        "part_population_canton",
        "population_dgf",
        "population_dgf_agglomeration",
        "population_dgf_departement_agglomeration",
        "population_insee",
        "potentiel_financier",
        "potentiel_financier_par_habitant",
        "revenu_total",
        "strate_demographique",
        "zrr",
        "effort_fiscal",
        "longueur_voirie",
        "zone_de_montagne",
        "insulaire",
        "superficie",
        "population_enfants",
        "nombre_logements",
        "nombre_logements_sociaux",
        "nombre_beneficiaires_aides_au_logement",
        "population_qpv",
        "population_zfu",
        "population_dgf_majoree",
        "recettes_reelles_fonctionnement",
        "potentiel_fiscal",
        "population_dgf_maximum_commune_agglomeration",
        "outre_mer",
        "population_dgf_chef_lieu_de_canton",
        "revenu_par_habitant_moyen",
        "dsu_montant_garantie_pluriannuelle",
        "dsr_garantie_commune_nouvelle_fraction_bourg_centre",
        "dsr_garantie_commune_nouvelle_fraction_perequation",
        "dsr_garantie_commune_nouvelle_fraction_cible",
    ]
    assert list(set(mandatory_columns) - set(list(fully_adapted_data_2022))) == []


def test_simulation_data_float_types():
    bool_cols = [
        "part_population_canton",
        "potentiel_financier",
        "potentiel_financier_par_habitant",
        "revenu_total",
        "effort_fiscal",
        "longueur_voirie",
        "superficie",
        "recettes_reelles_fonctionnement",
        "potentiel_fiscal",
        "revenu_par_habitant_moyen",
        "dsu_montant_garantie_pluriannuelle",
        "dsr_garantie_commune_nouvelle_fraction_bourg_centre",
        "dsr_garantie_commune_nouvelle_fraction_perequation",
        "dsr_garantie_commune_nouvelle_fraction_cible",
        "population_dgf_majoree",
    ]
    for col in bool_cols:
        assert fully_adapted_data_2022[col].dtype == float


def test_simulation_data_bool_types():
    bool_cols = [
        "bureau_centralisateur",
        "chef_lieu_arrondissement",
        "chef_lieu_de_canton",
        "chef_lieu_departement_dans_agglomeration",
        "zrr",
        "zone_de_montagne",
        "insulaire",
    ]
    for col in bool_cols:
        assert fully_adapted_data_2022[col].dtype == bool


def test_simulation_data_int_types():
    bool_cols = [
        "population_dgf",
        "population_dgf_agglomeration",
        "population_dgf_departement_agglomeration",
        "population_insee",
        "strate_demographique",
        "population_enfants",
        "nombre_logements",
        "nombre_logements_sociaux",
        "nombre_beneficiaires_aides_au_logement",
        "population_qpv",
        "population_zfu",
        "population_dgf_maximum_commune_agglomeration",
        "population_dgf_chef_lieu_de_canton",
    ]
    for col in bool_cols:
        assert fully_adapted_data_2022[col].dtype == int


def test_corrige_pot_fin_strate():
    pot_fin_strate_dgcl = "Potentiel fiscal et financier des communes - Potentiel financier moyen par habitant de la strate"
    code_insee = "Informations générales - Code INSEE de la commune"
    data_2021 = dgcl_data_2021
    data_2021 = ajoute_appartenance_outre_mer(data_2021, "outre_mer")
    data_2021_corrige = corrige_potentiel_moyen_strate(
        data_2021, variables_openfisca_presentes_fichier_2021, "outre_mer"
    )
    data_2021 = data_2021[
        [
            code_insee,
            pot_fin_strate_dgcl,
        ]
    ]
    data_2021_corrige = data_2021_corrige[[code_insee, "pot_fin_strate"]]
    comparison = data_2021.merge(
        data_2021_corrige,
        left_on=code_insee,
        right_on=code_insee,
    )
    comparison[pot_fin_strate_dgcl] = comparison[pot_fin_strate_dgcl].astype(float)
    comparison["difference"] = (
        comparison["pot_fin_strate"] - comparison[pot_fin_strate_dgcl]
    )

    # On s'assure que les cas où la différence est > 0,1€ (donc significative) sont en fait des cas où la DGCL n'a pas fourni la donnée
    assert comparison[
        (comparison["difference"] > 0.1) & (comparison[pot_fin_strate_dgcl] > 0)
    ].empty
