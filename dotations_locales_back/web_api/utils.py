from dotations_locales_back.common.mapping.commune.dotation_forfaitaire import *
from dotations_locales_back.common.mapping.commune.dotation_amorcage import *
from dotations_locales_back.common.mapping.commune.dotation_solidarite_urbaine import *
from dotations_locales_back.common.mapping.commune.dotation_nationale_perequation import *
from dotations_locales_back.common.mapping.commune.dotation_solidarite_rurale import *
from dotations_locales_back.common.mapping.commune.dotation_amenagement_communes_outre_mer import *
from dotations_locales_back.common.mapping.commune.dotation_elu_local import *
from dotations_locales_back.common.mapping.commune.dotation_biodiversite import (
    com_dotation_biodiversite_unites_criteres,
    com_dotation_biodiversite_sous_dotations,
)
from dotations_locales_back.common.mapping.commune.criteres_generaux import (
    unites_criteres_generaux as unites_criteres_generaux_commune,
)

from dotations_locales_back.common.mapping.epci.dotation_compensation import *
from dotations_locales_back.common.mapping.epci.dotation_groupements_touristiques import *
from dotations_locales_back.common.mapping.epci.dotation_intercommunalite import *

from dotations_locales_back.common.mapping.departement.dotation_compensation import *
from dotations_locales_back.common.mapping.departement.dotation_fonctionnement_minimale import *
from dotations_locales_back.common.mapping.departement.dotation_forfaitaire import *
from dotations_locales_back.common.mapping.departement.dotation_perequation_urbaine import *

from dotations_locales_back.common.utils import (
    search_entity_by_code,
    return_amounts_sum,
)

from dotations_locales_back.simulation.simulation import openfisca_simulation
import datetime


def create_init_response_echelon(echelon_territorial):
    if echelon_territorial == "commune":
        return {
            "dotation_forfaitaire": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - DF toutes communes (2023)",
                        "nom_fichier": "commune_dotations_forfaitaires_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=188",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046873855/2023-01-01",
                    },
                ],
            },
            "dsu_montant": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - DSU toutes communes (2023)",
                        "nom_fichier": "commune_dotations_solidarite_urbaine_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=179",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070633/LEGISCTA000006197674/#LEGISCTA000006197674",
                    },
                ],
            },
            "dotation_nationale_perequation": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - DNP toutes communes (2023)",
                        "nom_fichier": "commune_dotation_nationale_perequation_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=190",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044980613/2023-01-01",
                    },
                ],
            },
            "dotation_solidarite_rurale": {
                "sous_dotations": [
                    {
                        "dsr_fraction_bourg_centre": {
                            "fichiers": {
                                "national_montants": {
                                    "label": "DGF - DSR Fraction Bourg-centre toutes communes (2023)",
                                    "nom_fichier": "commune_dotations_solidarite_rurale_bourg_centre_2023.csv",
                                }
                            },
                        }
                    },
                    {
                        "dsr_fraction_perequation": {
                            "fichiers": {
                                "national_montants": {
                                    "label": "DGF - DSR Fraction Péréquantion toutes communes (2023)",
                                    "nom_fichier": "commune_dotations_solidarite_rurale_perequation_2023.csv",
                                }
                            }
                        },
                    },
                    {
                        "dsr_fraction_cible": {
                            "fichiers": {
                                "national_montants": {
                                    "label": "DGF - DSR Fraction Cible toutes communes (2023)",
                                    "nom_fichier": "commune_dotations_solidarite_rurale_cible_2023.csv",
                                }
                            }
                        }
                    },
                ],
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=181",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/id/LEGIARTI000046873852/2023-01-01/",
                    },
                ],
            },
            "dotation_amenagement_communes_outre_mer": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - DACOM toutes communes (2023)",
                        "nom_fichier": "commune_dotation_amenagement_communes_outre_mer_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=168",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070633/LEGISCTA000041426784/#LEGISCTA000041426784",
                    },
                ],
            },
            "dotation_elu_local": {  # Hors DGF
                "fichiers": {
                    "national_montants": {
                        "label": "DPEL toutes communes (2023)",
                        "nom_fichier": "commune_dotation_elu_local_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=165",
                    }
                ],
            },
            "dotation_biodiversite": {  # Hors DGF
                "fichiers": {
                    "national_montants": {
                        "label": "Dotation biodiversité et aménités rurales toutes communes (2023)",
                        "nom_fichier": "commune_dotation_biodiversite_2023.csv",
                    }
                },
                "liens_externes": [],
            },
        }
    elif echelon_territorial == "epci":
        return {
            "dotation_compensation": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - Dotation de compensation tous EPCI (2023)",
                        "nom_fichier": "epci_dotation_compensation_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=173",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070633/LEGISCTA000006192441/#LEGISCTA000006192441",
                    },
                ],
            },
            "dotation_groupements_touristiques": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - Dotation des groupements touristiques tous EPCI (2023)",
                        "nom_fichier": "epci_dotation_groupements_touristiques_2023.csv",
                    }
                },
                "liens_externes": [],
            },
            "dotation_intercommunalite": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - Dotation d'intercommunalité (DI) tous EPCI (2023)",
                        "nom_fichier": "epci_dotation_intercommunalite_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=182",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070633/LEGISCTA000006192441/#LEGISCTA000006192441",
                    },
                ],
            },
        }
    elif echelon_territorial == "departement":
        return {
            "dotation_compensation": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - Dotation de compensation tous départements (2023)",
                        "nom_fichier": "departement_dotation_compensation_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=183",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070633/LEGISCTA000006192583/#LEGISCTA000006192583",
                    },
                ],
            },
            "dotation_forfaitaire": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - DF tous départements (2023)",
                        "nom_fichier": "departement_dotation_forfaitaire_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=183",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070633/LEGISCTA000006192360/#LEGISCTA000006192360",
                    },
                ],
            },
            "dotation_perequation_urbaine": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - DPU tous départements (2023)",
                        "nom_fichier": "departement_dotation_perequation_urbaine_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=183",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070633/LEGISCTA000006192361/#LEGISCTA000006192361",
                    },
                ],
            },
            "dotation_fonctionnement_minimale": {
                "fichiers": {
                    "national_montants": {
                        "label": "DGF - Dotation de fonctionnement minimale tous départements (2023)",
                        "nom_fichier": "departement_dotation_fonctionnement_minimale_2023.csv",
                    }
                },
                "liens_externes": [
                    {
                        "label": "Note DGCL",
                        "url": "http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=183",
                    },
                    {
                        "label": "Article Legifrance",
                        "url": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044980696",
                    },
                ],
            },
        }
    else:
        return {}


def get_entity_libelle(code, **entities_datas):
    """
    Returns the name of an entity with the given code.

    Parameters:
        code (str): The code of the entity to retrieve the name for.
        **entities_datas: A dictionary containing DataFrames with entity data.

    Returns:
        str: The name of the entity with the given code.
    """
    current_year = datetime.datetime.now().year
    if (
        str(current_year) in entities_datas
        and entities_datas.get(str(current_year)) is not None
    ):
        year = current_year
    elif (
        str(current_year - 1) in entities_datas
        and entities_datas.get(str(current_year - 1)) is not None
    ):
        year = current_year - 1
    # When "simulation" is in entities_data, we use the current year : it is the case when the simulation endpoint is called
    elif (
        "simulation" in entities_datas and entities_datas.get("simulation") is not None
    ):
        year = current_year
    else:
        return None

    return (
        entities_datas.get(str(year))
        .loc[entities_datas.get(str(year))["code"] == code]["nom"]
        .values[0]
    )


def create_criteres_structure(unites_criteres, **entities_datas):
    """Permet de créer un dictionnaire des critères nécessaire pour créer le body de réponse des endpoints.
    Convertit aussi les données brutes venant du tableau DGCL au format attendu en sortie.
    Paramètres :
     - unites_criteres : dictionnaire listant les critères et leur unité
     - entities_datas : Dictionnaire contenant les Dataframes avec les données criteres / dotations par années"""
    criteres = {}
    for critere in unites_criteres.keys():
        year_list = []
        # Si le critères est un booléen, on remplace par Oui ou par Non.
        if unites_criteres[critere] == "boolean":
            unit = None
            for year in entities_datas:
                # Si le critère n'est pas présent dans la liste des colonnes du dataframe, on ne renvoie rien
                if critere in list(entities_datas.get(year).columns):
                    value = (
                        "Oui" if entities_datas.get(year)[critere].iloc[0] else "Non"
                    )
                    year_list.append({year: {"valeur": value, "unite": unit}})

        # Pour tous les autres critères, on convertit seuelement en string la valeur
        else:
            unit = unites_criteres[critere]
            for year in entities_datas:
                # Si le critère n'est pas présent dans la liste des colonnes du dataframe, on ne renvoie rien
                if critere in list(entities_datas.get(year).columns):
                    value = str(entities_datas.get(year)[critere].iloc[0])
                    year_list.append({year: {"valeur": value, "unite": unit}})
        criteres[critere] = {"annees": year_list}

    return criteres


def create_dotation_structure(dotation, entity_type, **entities_datas):
    """Permet de créer un dictionnaire contenant le détail d'une dotation nécessaire pour créer le body de réponse.
    Paramètres :
     - dotation : Le nom de la dotation à traiter
    - entities_datas : Dictionnaire contenant les Dataframes avec les données criteres / dotations par années"""

    year_list = []
    for year in entities_datas:
        year_list.append({year: entities_datas.get(year)[dotation].iloc[0]})
    dot = {
        "annees": year_list,
    }
    unites_criteres = eval(entity_type + "_" + dotation + "_unites_criteres")

    if len(unites_criteres.keys()) != 0:
        dot["criteres"] = create_criteres_structure(unites_criteres, **entities_datas)
    return dot


def create_dotations_structure(dotations_list, entity_type, **entities_datas):
    """Permet de créer un dictionnaire contenant toutes les dotations nécessaire pour créer le body de réponse des endpoints permettant de renvoyer les données d'une collectivité.
    Paramètres :
     - dotations_list : Liste des dotations à servir en réponse
     - entities_datas : Dictionnaire contenant les Dataframes avec les données criteres / dotations par années"""
    dotations = {}
    for dotation in dotations_list:
        dotations[dotation] = create_dotation_structure(
            dotation, entity_type, **entities_datas
        )
        sous_dotations = eval(entity_type + "_" + dotation + "_sous_dotations")
        if len(sous_dotations) != 0:
            dotations[dotation]["sous_dotations"] = []
            for sous_dotation in sous_dotations:
                dotations[dotation]["sous_dotations"].append(
                    {
                        sous_dotation: create_dotation_structure(
                            sous_dotation, entity_type, **entities_datas
                        ),
                    }
                )

    return dotations


def create_entity_response(
    code, dotations_list, unites_criteres_generaux, entity_type, **entities_datas
):
    """Permet de créer la réponse de l'endpoint /commune.
    Paramètres :
     - code : Code de la collectivité passé en paramètre de l'appel
     - dotations_list : Liste des dotations à servir en réponse
     - unites_criteres_generaux : Liste des critères généraux à renvoyer avec leur unité
     - entity_type : TODO
     - entities_datas : Dictionnaire contenant les Dataframes avec les données criteres / dotations par années"""
    year_list = []
    for year in entities_datas:
        if "part_dotation_rrf" in list(entities_datas.get(year).columns):
            value = str(entities_datas.get(year)["part_dotation_rrf"].iloc[0])
            year_list.append({year: {"valeur": value, "unite": "%"}})
    libelle = get_entity_libelle(code, **entities_datas)
    response = {
        "code": code,
        "libelle": libelle,
        "dotations": create_dotations_structure(
            dotations_list, entity_type, **entities_datas
        ),
        "criteres_generaux": create_criteres_structure(
            unites_criteres_generaux, **entities_datas
        ),
        "part_dotation_rrf": {"annees": year_list},
    }
    return response


def create_simulation_response(
    code_insee,
    simulation_parameters,
    dotations_criteres,
    dotations_list,
    periode_loi,
    avertissement_liste,
):
    simulation = openfisca_simulation(
        code_insee,
        simulation_parameters,
        dotations_criteres,
    )
    simulated_data = search_entity_by_code(simulation, code_insee)
    commune_data = search_entity_by_code(dotations_criteres, code_insee)
    communes_datas = {
        "simulation": simulated_data,
        periode_loi: commune_data,
    }
    response = create_entity_response(
        code_insee,
        dotations_list,
        unites_criteres_generaux_commune,
        "com",
        **communes_datas
    )
    if code_insee in avertissement_liste:
        response["avertissement_precision_simulation"] = True
    return response


def get_entity_data_by_year(
    code, past_entities, year, entity_dotations_criteres, dotations_list
):
    """Permet de récupérer les données des dotations et des critères par année pour une collectivité
    Paramètres :
     - code : Code de la collectivité
     - past_entities : Liste des codes des collectivités qui ont fusionné pour créer la collectivité passée en paramètre
     - year : Année pour laquelle on souhaite récupérer les données
     - entity_dotations_criteres : Dictionnaire contenant les Dataframes avec les données criteres / dotations par années
     - dotations_list : Liste des dotations à servir en réponse
    Retourne :
        - entity_data : Dataframe contenant les données des dotations et des critères pour la collectivité et l'année passées en paramètre
    """

    # Cas d'une commune nouvelle qui est le fruit d'une fusion de plusieurs communes. On fait la somme des dotations, mais on ne fait pas la somme des critères.
    if len(past_entities) > 1:
        entity_data = return_amounts_sum(
            entity_dotations_criteres.get(year), past_entities
        )
        # On supprime les colonnes des critères qui ne sont pas forcément des sommes
        entity_data = entity_data[dotations_list]
    # Cas d'une commune nouvelle qui a été créée à partir d'une seule commune. On récupère les données de cette commune.
    elif len(past_entities) == 1:
        entity_data = search_entity_by_code(
            entity_dotations_criteres.get(year), past_entities[0]
        )
    # Cas d'une commune qui n'est pas une commune nouvelle.
    else:
        entity_data = search_entity_by_code(entity_dotations_criteres.get(year), code)

    return entity_data


def delete_criteres_from_response(response: dict, dotations_list: list):
    response["criteres_generaux"] = None
    for dotation in dotations_list:
        response["dotations"][dotation]["criteres"] = None
        if "sous_dotations" in response["dotations"][dotation].keys():
            for sous_dotation in response["dotations"][dotation]["sous_dotations"]:
                for sous_dotation_name in sous_dotation.keys():
                    sous_dotation[sous_dotation_name]["criteres"] = None
        response["part_dotation_rrf"] = None
    return response
