from __future__ import annotations
from fastapi import FastAPI, HTTPException
from fastapi.responses import FileResponse
from typing import List, Dict, Union
import pandas as pd
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import os
import json
import requests

from dotations_locales_back.common import EchelonTerritorial
from dotations_locales_back.common.init_datas import init_dotations_data
from dotations_locales_back.common.utils import (
    search_entity_by_code,
    get_entity_type_by_code,
    get_territory_past_filiation,
    return_amounts_sum,
)
from dotations_locales_back.web_api.utils import (
    create_init_response_echelon,
    create_entity_response,
    create_simulation_response,
    delete_criteres_from_response,
    get_entity_data_by_year,
)
from dotations_locales_back.common.mapping.commune.criteres_generaux import (
    unites_criteres_generaux as unites_criteres_generaux_commune,
)
from dotations_locales_back.common.mapping.epci.criteres_generaux import (
    unites_criteres_generaux as unites_criteres_generaux_epci,
)
from dotations_locales_back.common.mapping.departement.criteres_generaux import (
    unites_criteres_generaux_met as unites_criteres_generaux_departement_met,
    unites_criteres_generaux_dom as unites_criteres_generaux_departement_dom,
    unites_criteres_generaux_com as unites_criteres_generaux_departement_com,
)

DATA_DIRECTORY = os.getcwd() + "/data/"

# Consulte la variable d'environnement "CRITERES" pour savoir si on doit renvoyer les critères ou non. Si la variable n'est pas définie, on renvoie les critères.
CRITERES = os.getenv("CRITERES", "true") == "true"

print("CRITERES", CRITERES)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["GET", "POST"],
)


(
    commune_dotations_criteres,
    liste_communes_avertissement,
    epci_dotations_criteres,
    departement_dotations_criteres,
) = init_dotations_data()


COMMUNE_DOTATIONS_LIST = [
    "dotation_forfaitaire",
    "dotation_amorcage",
    "dsu_montant",
    "dotation_nationale_perequation",
    "dotation_solidarite_rurale",
    "dotation_amenagement_communes_outre_mer",
    "dotation_elu_local",
    "dotation_biodiversite",
]

EPCI_DOTATIONS_LIST = [
    "dotation_compensation",
    "dotation_groupements_touristiques",
    "dotation_intercommunalite",
]

DEPARTEMENT_DOTATIONS_LIST = [
    "dotation_compensation",
    "dotation_forfaitaire",
    "dotation_perequation_urbaine",
    "dotation_fonctionnement_minimale",
]


@app.get("/")
async def root():
    return {
        "message": "Ceci est l'API de l'application Dotations Locales, pour voir la documentation ajoutez /docs sur votre URL",
        "api_version": "2.7.0",
    }


class Fichier(BaseModel):
    label: str
    nom_fichier: str


class FichiersDotation(BaseModel):
    national_montants: Union[Fichier, None]
    national_criteres: Union[Fichier, None]
    sous_dotations: Union[List[Dict[str, FichiersDotation]], None]


class Lien(BaseModel):
    label: str
    url: str


class Donnees(BaseModel):
    liens_externes: Union[List[Lien], None] = None
    fichiers: Union[FichiersDotation, None] = None


class CommuneDonnees(BaseModel):
    dotation_forfaitaire: Donnees
    dsu_montant: Donnees
    dotation_nationale_perequation: Donnees
    dotation_solidarite_rurale: Donnees
    dotation_amenagement_communes_outre_mer: Donnees
    dotation_elu_local: Donnees
    dotation_biodiversite: Donnees


class EpciDonnees(BaseModel):
    dotation_compensation: Donnees
    dotation_groupements_touristiques: Donnees
    dotation_intercommunalite: Donnees


class DepartementDonnees(BaseModel):
    dotation_compensation: Donnees
    dotation_forfaitaire: Donnees
    dotation_perequation_urbaine: Donnees
    dotation_fonctionnement_minimale: Donnees


class Echelon(BaseModel):
    commune: CommuneDonnees
    epci: EpciDonnees
    departement: DepartementDonnees


class SimulationPeriode(BaseModel):
    annee: str
    label: str


class InitResponse(BaseModel):
    simulation_periodes: List[SimulationPeriode]
    base_calcul: str
    sources_donnees: Echelon
    derniere_maj_donnees: str


@app.get("/init", response_model=InitResponse)
async def init():
    return {
        "derniere_maj_donnees": "2023-05-31",
        "base_calcul": "Loi en vigueur 2023",
        "simulation_periodes": [
            {
                "annee": 2022,
                "label": "Règlementation 2022",
            }
        ],
        "sources_donnees": {
            "commune": create_init_response_echelon("commune"),
            "epci": create_init_response_echelon("epci"),
            "departement": create_init_response_echelon("departement"),
        },
    }


@app.get("/files/{filename}")
def download_file(filename):
    """
    Permet de télécharger les fichiers présents dans /data/.
    Param : nom du fichier à mettre dans l'url
    """
    file_path = DATA_DIRECTORY + filename
    if os.path.exists(file_path):
        return FileResponse(path=file_path, filename=filename)
    else:
        raise HTTPException(status_code=404, detail="Fichier inexistant")


class CommuneRequest(BaseModel):
    code: str


class ValeurCritere(BaseModel):
    valeur: Union[str, None] = None
    unite: Union[str, None] = None


class Montant(BaseModel):
    __root__: Union[float, None] = None


class ValeursCritereParAnnee(BaseModel):
    annees: List[Dict[str, Union[ValeurCritere, None]]]


class Dotation(BaseModel):
    annees: List[Dict[str, Montant]]
    criteres: Union[Dict[str, ValeursCritereParAnnee], None] = None
    sous_dotations: Union[List[Dict[str, Dotation]], None] = None


class CommuneDotations(BaseModel):
    dotation_forfaitaire: Dotation
    dotation_amorcage: Dotation
    dsu_montant: Dotation
    dotation_nationale_perequation: Dotation
    dotation_solidarite_rurale: Dotation
    dotation_amenagement_communes_outre_mer: Dotation
    dotation_elu_local: Dotation
    dotation_biodiversite: Dotation


class CriteresGenerauxCommune(BaseModel):
    population_insee: ValeursCritereParAnnee
    population_dgf: ValeursCritereParAnnee
    strate_demographique: ValeursCritereParAnnee
    potentiel_financier: ValeursCritereParAnnee
    potentiel_financier_par_habitant: ValeursCritereParAnnee
    effort_fiscal: ValeursCritereParAnnee
    recettes_reelles_fonctionnement: ValeursCritereParAnnee
    epci: ValeursCritereParAnnee


class CommuneResponse(BaseModel):
    """
    Les éléments exacts de la réponse.
    Tout autre élément sera ignoré.
    """

    code: str
    libelle: str
    dotations: CommuneDotations
    criteres_generaux: Union[CriteresGenerauxCommune, None] = None
    part_dotation_rrf: Union[ValeursCritereParAnnee, None] = None


def get_commune_data(code, commune_dotations_criteres):
    communes_datas = {}
    year_list = list(commune_dotations_criteres.keys())
    max_year = year_list[0]
    commune_all_dotations_list = COMMUNE_DOTATIONS_LIST + [
        "dsr_fraction_bourg_centre",
        "dsr_fraction_cible",
        "dsr_fraction_perequation",
    ]
    for year in year_list:
        past_entities = []
        if year != max_year:
            past_entities = get_territory_past_filiation(
                EchelonTerritorial.COMMUNE, code, int(max_year), int(year)
            )

        commune_data = get_entity_data_by_year(
            code,
            past_entities,
            year,
            commune_dotations_criteres,
            commune_all_dotations_list,
        )

        if (
            commune_data is not None
            and (
                isinstance(commune_data, pd.DataFrame)
                or isinstance(commune_data, pd.Series)
            )
            and not commune_data.empty
        ):
            communes_datas.update({year: commune_data})
        else:
            raise HTTPException(status_code=404, detail="Commune non trouvée")

    return communes_datas


@app.post("/commune/", response_model=CommuneResponse)
async def describe_commune(commune: CommuneRequest):
    try:
        communes_datas = get_commune_data(commune.code, commune_dotations_criteres)
    except HTTPException as e:
        raise e

    response = create_entity_response(
        commune.code,
        COMMUNE_DOTATIONS_LIST,
        unites_criteres_generaux_commune,
        "com",
        **communes_datas,
    )

    if CRITERES == False:
        response = delete_criteres_from_response(response, COMMUNE_DOTATIONS_LIST)

    return response


class EpciRequest(BaseModel):
    code: str


class EpciDotations(BaseModel):
    dotation_compensation: Dotation
    dotation_groupements_touristiques: Dotation
    dotation_intercommunalite: Dotation


class CriteresGenerauxEpci(BaseModel):
    population_insee: ValeursCritereParAnnee
    departement: ValeursCritereParAnnee
    nature_juridique: ValeursCritereParAnnee
    regime_fiscal: ValeursCritereParAnnee
    potentiel_fiscal: ValeursCritereParAnnee
    potentiel_fiscal_par_habitant: ValeursCritereParAnnee
    coefficient_integration_fiscale: ValeursCritereParAnnee
    recettes_reelles_fonctionnement: ValeursCritereParAnnee


class EpciResponse(BaseModel):
    code: str
    libelle: str
    dotations: EpciDotations
    criteres_generaux: Union[CriteresGenerauxEpci, None] = None
    part_dotation_rrf: Union[ValeursCritereParAnnee, None] = None


def get_epci_data(code, epci_dotations_criteres):
    epci_datas = {}
    epci_found = False
    for year in epci_dotations_criteres.keys():
        epci_data = search_entity_by_code(epci_dotations_criteres.get(year), code)
        if (
            epci_data is not None
            and isinstance(epci_data, pd.DataFrame)
            and not epci_data.empty
        ):
            epci_found = True
            epci_datas.update({year: epci_data})
    if epci_found == False:
        raise HTTPException(status_code=404, detail="EPCI non trouvé")

    return epci_datas


@app.post("/epci/", response_model=EpciResponse)
async def describe_epci(epci: EpciRequest):
    try:
        epci_datas = get_epci_data(epci.code, epci_dotations_criteres)
    except HTTPException as e:
        raise e

    response = create_entity_response(
        epci.code,
        EPCI_DOTATIONS_LIST,
        unites_criteres_generaux_epci,
        "epci",
        **epci_datas,
    )

    if CRITERES == False:
        response = delete_criteres_from_response(response, EPCI_DOTATIONS_LIST)
    return response


class DepartementRequest(BaseModel):
    code: str


class DepartementDotations(BaseModel):
    dotation_compensation: Dotation
    dotation_forfaitaire: Dotation
    dotation_perequation_urbaine: Dotation
    dotation_fonctionnement_minimale: Dotation


class CriteresGenerauxDepartement(BaseModel):
    population_insee: ValeursCritereParAnnee
    population_dgf: ValeursCritereParAnnee
    departement_urbain: Union[ValeursCritereParAnnee, None] = None
    revenu_par_habitant: Union[ValeursCritereParAnnee, None] = None
    taux_urbanisation: Union[ValeursCritereParAnnee, None] = None
    densite_population: Union[ValeursCritereParAnnee, None] = None
    voirie_hors_montagne: Union[ValeursCritereParAnnee, None] = None
    voirie_montagne: Union[ValeursCritereParAnnee, None] = None
    logements_TH: Union[ValeursCritereParAnnee, None] = None
    beneficiaires_apl: Union[ValeursCritereParAnnee, None] = None
    beneficiaires_rsa: Union[ValeursCritereParAnnee, None] = None
    potentiel_financier: Union[ValeursCritereParAnnee, None] = None
    potentiel_financier_par_habitant: Union[ValeursCritereParAnnee, None] = None


class DepartementResponse(BaseModel):
    code: str
    libelle: str
    dotations: DepartementDotations
    criteres_generaux: Union[CriteresGenerauxDepartement, None] = None
    part_dotation_rrf: Union[ValeursCritereParAnnee, None] = None


def get_departement_data(code, departement_dotations_criteres):
    departement_datas = {}
    departement_found = True
    for year in departement_dotations_criteres.keys():
        departement_data = search_entity_by_code(
            departement_dotations_criteres.get(year), code
        )
        if (
            departement_data is not None
            and isinstance(departement_data, pd.DataFrame)
            and not departement_data.empty
        ):
            departement_datas.update({year: departement_data})
        else:
            departement_found = False
    if departement_found == False:
        raise HTTPException(status_code=404, detail="Département non trouvé")

    return departement_datas


def get_criteres_generaux_departement(code):
    """Cette fonction détermine si le département est de métropole, de DOM ou de COM.
    En fonction, les critères généraux sont différents et elle renvoie les critères généraux correspondants.
    Paramètres :
        - code : code du département
    """
    # Si c'est un DOM/COM (commence par 97 et fait 3 caractères)
    if code.startswith("97") and len(code) > 2:
        # Si le code fait partie de la liste des COM disponibles (St-Pierre-et-Miquelon et Saint-Martin)
        if code in ["975", "978"]:
            unites_criteres_generaux_departement = (
                unites_criteres_generaux_departement_com
            )
        # Autrement c'est un DOM
        else:
            unites_criteres_generaux_departement = (
                unites_criteres_generaux_departement_dom
            )
    # Autrement est un département de métropole
    else:
        unites_criteres_generaux_departement = unites_criteres_generaux_departement_met
    return unites_criteres_generaux_departement


@app.post("/departement/", response_model=DepartementResponse)
async def describe_departement(departement: DepartementRequest):
    try:
        departement_datas = get_departement_data(
            departement.code, departement_dotations_criteres
        )
    except HTTPException as e:
        raise e

    unites_criteres_generaux_departement = get_criteres_generaux_departement(
        departement.code
    )

    response = create_entity_response(
        departement.code,
        DEPARTEMENT_DOTATIONS_LIST,
        unites_criteres_generaux_departement,
        "dpt",
        **departement_datas,
    )
    if CRITERES == False:
        response = delete_criteres_from_response(response, DEPARTEMENT_DOTATIONS_LIST)

    return response


class CriteresSimulation(BaseModel):
    population_insee: int
    population_dgf: int
    potentiel_financier: float
    potentiel_financier_par_habitant: float
    effort_fiscal: float
    recettes_reelles_fonctionnement: float


class SimulationRequest(BaseModel):
    code: str
    periode_loi: str
    data: CriteresSimulation


class SimulationResponse(BaseModel):
    """
    Les éléments exacts de la réponse.
    Tout autre élément sera ignoré.
    """

    code: str
    libelle: str
    dotations: CommuneDotations
    criteres_generaux: CriteresGenerauxCommune
    avertissement_precision_simulation: bool = False


@app.post("/simulation/", response_model=SimulationResponse)
async def commune_simulation(simulation_parameters: SimulationRequest):
    # scenario usager
    # de janvier à juillet de l'année N, on a la loi de N et les critères de N-1
    # de juillet à l'automne (PLF) de l'année N, on la loi de N et les critères de N
    # à l'automne (PLF) de l'année N+1, on a la loi estimée (PLF) de N+1 et les critères N

    simulation_parameters_dict = simulation_parameters.dict()
    if simulation_parameters.periode_loi == "2022":
        commune_data_2022 = search_entity_by_code(
            commune_dotations_criteres.get(simulation_parameters.periode_loi),
            simulation_parameters.code,
        )
        if (
            commune_data_2022 is not None
            and isinstance(commune_data_2022, pd.DataFrame)
            and not commune_data_2022.empty
        ):

            response = create_simulation_response(
                simulation_parameters.code,
                simulation_parameters_dict["data"],
                commune_dotations_criteres.get(simulation_parameters.periode_loi),
                COMMUNE_DOTATIONS_LIST,
                simulation_parameters.periode_loi,
                liste_communes_avertissement,
            )
        else:
            raise HTTPException(status_code=404, detail="Commune non trouvée")
    elif simulation_parameters.periode_loi == "2021":
        commune_data_2021 = search_entity_by_code(
            commune_dotations_criteres.get(simulation_parameters.periode_loi),
            simulation_parameters.code,
        )
        if (
            commune_data_2021 is not None
            and isinstance(commune_data_2021, pd.DataFrame)
            and not commune_data_2021.empty
        ):
            response = create_simulation_response(
                simulation_parameters.code,
                simulation_parameters_dict["data"],
                commune_dotations_criteres.get(simulation_parameters.periode_loi),
                COMMUNE_DOTATIONS_LIST,
                simulation_parameters.periode_loi,
                liste_communes_avertissement,
            )
        else:
            raise HTTPException(status_code=404, detail="Commune non trouvée")

    else:
        raise HTTPException(
            status_code=400,
            detail="'periode' must be 2021 or 2022, others years are not supported",
        )

    return response


class AlerteRequest(BaseModel):
    email: str
    code: str
    entity: str


@app.post("/alerte/", status_code=201)
async def create_alerte(alerte_parameters: AlerteRequest):
    alerte_parameters_dict = alerte_parameters.dict()
    sib_apikey = os.environ.get("SENDINBLUE_APIKEY")
    url = "https://api.sendinblue.com/v3/contacts/"

    payload = json.dumps(
        {
            "email": alerte_parameters_dict["email"],
            "attributes": {
                "CODEDL": alerte_parameters_dict["code"],
                "COMMUNE": alerte_parameters_dict["entity"],
            },
            "listIds": [121],
        }
    )
    headers = {
        "content-type": "application/json",
        "api-key": sib_apikey,
    }
    try:
        response = requests.request("POST", url, headers=headers, data=payload)
        response.raise_for_status()
    except requests.exceptions.HTTPError:
        if response.json()["code"] == "duplicate_parameter":
            raise HTTPException(
                status_code=403,
                detail="Email already exists",
            )
        else:
            raise HTTPException(
                status_code=response.status_code,
                detail=response.text,
            )
    return ""


class ComparaisonRequest(BaseModel):
    code: str


class CommuneComparaisonResponse(BaseModel):
    epci: List[CommuneResponse]
    departement: List[CommuneResponse]


class EpciComparaisonResponse(BaseModel):
    departement: List[EpciResponse]


class DepartementComparaisonResponse(BaseModel):
    region: List[DepartementResponse]


@app.post(
    "/comparaison/",
    response_model=Union[
        CommuneComparaisonResponse,
        EpciComparaisonResponse,
        DepartementComparaisonResponse,
    ],
)
async def commune_comparaison(comparaison_parameters: ComparaisonRequest):
    comparaison_parameters_dict = comparaison_parameters.dict()
    code = comparaison_parameters_dict["code"]
    echelon = get_entity_type_by_code(code)
    if echelon == EchelonTerritorial.COMMUNE:
        year_list = list(commune_dotations_criteres.keys())
        max_year = year_list[0]  # TO DO changer pour comparer avec année en cours
        first_two_keys = list(commune_dotations_criteres.keys())[:2]
        comparaison_commune_dotations_criteres = {
            k: v for k, v in commune_dotations_criteres.items() if k in first_two_keys
        }

        commune_data = get_entity_data_by_year(
            code,
            [],
            max_year,
            comparaison_commune_dotations_criteres,
            COMMUNE_DOTATIONS_LIST,
        )
        if not (
            commune_data is not None
            and (
                isinstance(commune_data, pd.DataFrame)
                or isinstance(commune_data, pd.Series)
            )
            and not commune_data.empty
        ):
            raise HTTPException(status_code=404, detail="Commune non trouvée")

        epcis_codes = commune_data["epci_commune_list"].iloc[0]
        dpts_codes = commune_data["dpt_commune_list"].iloc[0]
        epcis, dpts = [], []
        for entity_type, entity_codes in [("epci", epcis_codes), ("dpt", dpts_codes)]:
            for entity_code in entity_codes:
                try:
                    communes_datas = get_commune_data(
                        entity_code, comparaison_commune_dotations_criteres
                    )
                except HTTPException as e:
                    raise e
                response_model = create_entity_response(
                    entity_code,
                    COMMUNE_DOTATIONS_LIST,
                    unites_criteres_generaux_commune,
                    "com",
                    **communes_datas,
                )
                if entity_type == "epci":
                    epcis.append(response_model)
                else:
                    dpts.append(response_model)
        return CommuneComparaisonResponse(epci=epcis, departement=dpts)

    elif echelon == EchelonTerritorial.EPCI:
        year_list = list(epci_dotations_criteres.keys())
        max_year = year_list[0]  # TO DO changer pour comparer avec année en cours
        first_two_keys = list(epci_dotations_criteres.keys())[:2]
        comparaison_epci_dotations_criteres = {
            k: v for k, v in epci_dotations_criteres.items() if k in first_two_keys
        }
        epci_data = search_entity_by_code(
            comparaison_epci_dotations_criteres.get(max_year), code
        )
        if not (
            epci_data is not None
            and isinstance(epci_data, pd.DataFrame)
            and not epci_data.empty
        ):
            raise HTTPException(status_code=404, detail="Epci non trouvé")
        dpts_codes = epci_data["dpt_epci_list"].iloc[0]
        dpts = []
        for entity_code in dpts_codes:
            try:
                epci_datas = get_epci_data(
                    entity_code, comparaison_epci_dotations_criteres
                )
            except HTTPException as e:
                raise e
            response_model = create_entity_response(
                entity_code,
                EPCI_DOTATIONS_LIST,
                unites_criteres_generaux_epci,
                "epci",
                **epci_datas,
            )
            dpts.append(response_model)
        return EpciComparaisonResponse(departement=dpts)

    elif echelon == EchelonTerritorial.DEPARTEMENT:
        year_list = list(departement_dotations_criteres.keys())
        max_year = year_list[0]  # TO DO changer pour comparer avec année en cours
        first_two_keys = list(departement_dotations_criteres.keys())[:2]
        comparaison_departement_dotations_criteres = {
            k: v
            for k, v in departement_dotations_criteres.items()
            if k in first_two_keys
        }
        departement_data = search_entity_by_code(
            comparaison_departement_dotations_criteres.get(max_year), code
        )
        if not (
            departement_data is not None
            and isinstance(departement_data, pd.DataFrame)
            and not departement_data.empty
        ):
            raise HTTPException(status_code=404, detail="Département non trouvé")

        regions_codes = departement_data["region_dpt_list"].iloc[0]
        regions = []
        unites_criteres_generaux_departement = get_criteres_generaux_departement(code)

        for entity_code in regions_codes:
            try:
                departement_datas = get_departement_data(
                    entity_code, comparaison_departement_dotations_criteres
                )
            except HTTPException as e:
                raise e
            response_model = create_entity_response(
                entity_code,
                DEPARTEMENT_DOTATIONS_LIST,
                unites_criteres_generaux_departement,
                "dpt",
                **departement_datas,
            )
            regions.append(response_model)
        return DepartementComparaisonResponse(region=regions)

    else:
        raise HTTPException(status_code=404, detail="Code non valide")
