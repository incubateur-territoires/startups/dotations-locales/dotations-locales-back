filename_epci_criteres_2021 = "criteres_repartition_EPCI_2021.csv"

infos_generales_2021 = {
    "Informations générales - Numéro SIREN": "code",
    "Informations générales - Libellé EPCI": "nom",
    "Informations générales - Numéro département": "departement",
    "Informations générales - Nature juridique": "nature_juridique",
    "Informations générales - Régime fiscal": "regime_fiscal",
    # "Dotation d'intercommunalité - Revenu des EPCI": "recettes_reelles_fonctionnement" dans "revenu_epci"
}

montants_dotations_2021 = {
    "Dotation de compensation - Montant de la dotation de compensation des EPCI pour l'année N": "dotation_compensation",
    "Dotation d'intercommunalité - Dotation": "dotation_intercommunalite",
    "Dotation des groupements touristiques - Dotation des groupements touristiques": "dotation_groupements_touristiques",
}

criteres_2021 = {
    "Dotation d'intercommunalité - Population Insee de l'année N": "population_insee",
    "Dotation d'intercommunalité - Population DGF de l'année N": "population_dgf",
    "Dotation d'intercommunalité - Revenu des EPCI": "revenu_epci",
    "Dotation d'intercommunalité - Réalimentation": "realimentation",
    "Dotation d'intercommunalité - Dotation de base": "dotation_de_base",
    "Dotation d'intercommunalité - Dotation de péréquation": "dotation_de_perequation",
    "Dotation d'intercommunalité - Garantie": "garantie",
    "Dotation d'intercommunalité - Plafonnement": "plafonnement",
    "Potentiel fiscal - Potentiel fiscal": "potentiel_fiscal",
    "Potentiel fiscal - Potentiel fiscal/habitant": "potentiel_fiscal_par_habitant",
    "Coefficient d'intégration fiscale - CIF": "coefficient_integration_fiscale",
}

epci_columns_to_keep_2021 = {
    **infos_generales_2021,
    **montants_dotations_2021,
    **criteres_2021,
}
