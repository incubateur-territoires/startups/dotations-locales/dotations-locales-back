unites_criteres_generaux = {
    "population_insee": None,
    "departement": None,
    "nature_juridique": None,
    "regime_fiscal": None,
    "potentiel_fiscal": "€",
    "potentiel_fiscal_par_habitant": "€",
    "coefficient_integration_fiscale": None,
    "recettes_reelles_fonctionnement": "€",
}
