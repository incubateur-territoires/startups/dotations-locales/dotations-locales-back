filename_epci_criteres_2023 = "criteres_repartition_EPCI_2023.csv"

infos_generales_2023 = {
    "Informations générales - Numéro SIREN EPCI": "code",
    "Informations générales - Libellé EPCI": "nom",
    "Informations générales - Département siège de l'EPCI": "departement",
    "Informations générales - Nature juridique": "nature_juridique",
    "Informations générales - Régime fiscal": "regime_fiscal",
    # "Dotation d'intercommunalité - Revenu des EPCI": "recettes_reelles_fonctionnement" dans "revenu_epci"
}

montants_dotations_2023 = {
    "Dotation de compensation - Montant de la dotation de compensation des EPCI pour l'année N": "dotation_compensation",
    "Dotation d'intercommunalité - Montant de la dotation d'intercommunalité pour l'année N": "dotation_intercommunalite",
    "Dotation des groupements touristiques - Montant de la dotation des groupements touristiques pour l'année N": "dotation_groupements_touristiques",
}

criteres_2023 = {
    "Dotation d'intercommunalité - Population INSEE de l'année N": "population_insee",
    "Dotation d'intercommunalité - Population DGF de l'année N": "population_dgf",
    "Dotation d'intercommunalité - Revenu des EPCI": "revenu_epci",
    "Dotation d'intercommunalité - Réalimentation": "realimentation",
    "Dotation d'intercommunalité - Dotation de base": "dotation_de_base",
    "Dotation d'intercommunalité - Dotation de péréquation": "dotation_de_perequation",
    "Dotation d'intercommunalité - Garantie": "garantie",
    "Dotation d'intercommunalité - Plafonnement": "plafonnement",
    "Potentiel fiscal - Potentiel fiscal": "potentiel_fiscal",
    "Potentiel fiscal - Potentiel fiscal par habitant": "potentiel_fiscal_par_habitant",
    "CIF - CIF": "coefficient_integration_fiscale",
}


epci_columns_to_keep_2023 = {
    **infos_generales_2023,
    **montants_dotations_2023,
    **criteres_2023,
}
