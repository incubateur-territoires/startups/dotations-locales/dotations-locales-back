filename_departement_criteres_2022 = "criteres_repartition_departement_2022.csv"

infos_generales_2022 = {
    "Informations générales - Numéro département": "code",
    "Informations générales - Nom département": "nom",
    "Caractéristiques démographiques, physiques et financières - Département urbain": "departement_urbain",
    "Caractéristiques démographiques, physiques et financières - Population INSEE": "population_insee",
    "Caractéristiques démographiques, physiques et financières - Population DGF": "population_dgf",
    "Caractéristiques démographiques, physiques et financières - Revenu / habitant": "revenu_par_habitant",
    "Caractéristiques démographiques, physiques et financières - Taux d'urbanisation (grille de densité)": "taux_urbanisation",
    "Caractéristiques démographiques, physiques et financières - Densité de population en km2": "densite_population",
    "Caractéristiques démographiques, physiques et financières - Voirie hors montagne (en ml)": "voirie_hors_montagne",
    "Caractéristiques démographiques, physiques et financières - Voirie montagne (en ml)": "voirie_montagne",
    "Caractéristiques démographiques, physiques et financières - Nombre de logements soumis à TH": "logements_TH",
    "Caractéristiques démographiques, physiques et financières - Nombre de bénéficiaires des APL": "beneficiaires_apl",
    "Caractéristiques démographiques, physiques et financières - Nombre de foyers bénéficiaires du RSA": "beneficiaires_rsa",
    "Potentiel financier - Potentiel financier": "potentiel_financier",
    "Potentiel financier - Pfi / habitant": "potentiel_financier_par_habitant",
    "Dotation globale de fonctionnement - Recettes Réelles de Fonctionnement (comptes de gestion N-2)": "recettes_reelles_fonctionnement",
}

montants_dotations_2022 = {
    "Dotation globale de fonctionnement - Dotation de compensation": "dotation_compensation",
    "Dotation globale de fonctionnement - Dotation forfaitaire": "dotation_forfaitaire",
    "Dotation globale de fonctionnement - Dotation de péréquation urbaine": "dotation_perequation_urbaine",
    "Dotation globale de fonctionnement - Dotation de fonctionnement minimale": "dotation_fonctionnement_minimale",
}

criteres_2022 = {
    "Dotation globale de fonctionnement - Part dynamique de la population": "part_dynamique_population",
    "Dotation globale de fonctionnement - Ecrêtement de la dotation forfaitaire": "ecretement_dotation_forfaitaire",
}


departement_columns_to_keep_2022 = {
    **infos_generales_2022,
    **montants_dotations_2022,
    **criteres_2022,
}
