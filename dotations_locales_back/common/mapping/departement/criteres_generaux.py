unites_criteres_generaux_met = {
    "population_insee": None,
    "population_dgf": None,
    "departement_urbain": "boolean",
    "revenu_par_habitant": "€",
    "taux_urbanisation": None,
    "densite_population": "km2",
    "voirie_hors_montagne": "ml",
    "voirie_montagne": "ml",
    "logements_TH": None,
    "beneficiaires_apl": None,
    "beneficiaires_rsa": None,
    "potentiel_financier": "€",
    "potentiel_financier_par_habitant": "€",
}

unites_criteres_generaux_dom = {
    "population_insee": None,
    "population_dgf": None,
    "voirie_hors_montagne": "ml",
    "voirie_montagne": "ml",
    "potentiel_financier": "€",
    "potentiel_financier_par_habitant": "€",
}


unites_criteres_generaux_com = {
    "population_insee": None,
    "population_dgf": None,
}
