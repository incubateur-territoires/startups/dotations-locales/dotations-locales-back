com_dotation_biodiversite_unites_criteres = {}

com_dotation_biodiversite_sous_dotations = []

filename_commune_dbio_2023 = "commune_dotation_biodiversite_2023.csv"
filename_commune_dbio_2022 = "commune_dotation_biodiversite_2022.csv"
filename_commune_dbio_2021 = "commune_dotation_biodiversite_2021.csv"


columns_to_keep_commune_dbio_2023 = {
    "Code INSEE": "code",
    "Dotation de biodiversité 2022 totale": "dotation_biodiversite",
}

columns_to_keep_commune_dbio_2022 = {
    "Code INSEE": "code",
    "Dotation de biodiversité 2022 totale": "dotation_biodiversite",
}

columns_to_keep_commune_dbio_2021 = {
    "Code INSEE": "code",
    "Dotation de biodiversité 2021 totale": "dotation_biodiversite",
}
