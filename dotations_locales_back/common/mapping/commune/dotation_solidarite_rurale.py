com_dotation_solidarite_rurale_unites_criteres = {}

com_dotation_solidarite_rurale_sous_dotations = [
    "dsr_fraction_bourg_centre",
    "dsr_fraction_perequation",
    "dsr_fraction_cible",
]

com_dsr_fraction_bourg_centre_unites_criteres = {
    "part_population_canton": "%",
    "commune_chef_lieu_canton": None,
    "bureau_centralisateur": "boolean",
    "chef_lieu_arrondissement": "boolean",
    "chef_lieu_departement_dans_agglomeration": "boolean",
    "zrr": "boolean",
}

com_dsr_fraction_perequation_unites_criteres = {
    "population_enfants": None,
    "longueur_voirie": "m",
    "zone_de_montagne": "boolean",
    "insulaire": "boolean",
}

com_dsr_fraction_cible_unites_criteres = {
    "population_enfants": None,
    "longueur_voirie": "m",
    "zone_de_montagne": "boolean",
    "insulaire": "boolean",
    "rang_indice_synthetique": None,
}
