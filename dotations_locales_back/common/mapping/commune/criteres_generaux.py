unites_criteres_generaux = {
    "population_insee": None,
    "population_dgf": None,
    "strate_demographique": None,
    "epci": None,
    "potentiel_financier": "€",
    "potentiel_financier_par_habitant": "€",
    "effort_fiscal": None,
    "recettes_reelles_fonctionnement": "€",
}
