import logging
import numpy as np

from dotations_locales_back.common import EchelonTerritorial
from dotations_locales_back.common.load_filiation_data import get_filiation_data
from dotations_locales_back.simulation.simulation import calculate_dotations


logger = logging.getLogger(__name__)
logger.level = logging.DEBUG


def get_entity_type_by_code(code):
    if len(code) <= 3 and len(code) > 1:
        return EchelonTerritorial.DEPARTEMENT
    elif len(code) == 5:
        return EchelonTerritorial.COMMUNE
    elif len(code) == 9:
        return EchelonTerritorial.EPCI
    else:
        return None


def search_entity_by_code(data, code):
    """Renvoie la collectivité (commune, epci, département, etc..) correspondant au code passé en paramètre.
    Ne renvoie rien si la collectivité n'existe pas.
    Le code est différent selon le type de collectivité :
        Commune : code insee
        EPCI : numéro siren
    Paramètres :
        - data : dataframe contenant les données des dotations
        - code : code de la collectivité à rechercher"""

    # La Corse est un cas spécifique, puisque dans le fichier DGCL, il n'y a qu'une ligne "Corse" qui regroupe les 2 départements 2A et 2B
    corse_codes = ["2A", "2B", "20A", "20B", "20"]

    if code in corse_codes:
        code = "20A"

    try:
        entity = data.loc[data["code"] == code]
        if entity.empty:
            return None
    except:
        entity = None
    return entity


def return_amounts_sum(data, codes):
    """Renvoie la somme des montants des dotations des collectivités dont les codes sont passés en paramètre.
    Cette fonction sert essentiellement à sommer le montant des dotations des anciennes collectivités composant une collectivité nouvelle.
    Paramètres :
        - data : dataframe contenant les données des dotations
        - codes : liste des codes identifiant les collectivités"""

    try:
        entity = data.loc[data["code"].isin(codes)]
        if entity.empty:
            return None
        else:
            entity.loc["Column_Total"] = entity.sum(numeric_only=True, axis=0)
            entity = entity.drop(entity.loc[entity["code"].isin(codes)].index)
    except:
        entity = None
    return entity


def get_territory_past_filiation(
    echelon_territorial, territory_code, reference_year, target_year
):
    """
    :param territory_code: Code d'identification officiel d'un territoire
        (code INSEE pour les communes) pour l'année reference_year
    """
    logger.debug(
        f"Recherche de filiation pour {echelon_territorial.value} {territory_code}..."
    )
    logger.debug(f"Année de ce territoire : {reference_year}")
    logger.debug(f"Année cible des ancêtres recherchés : {target_year}")
    assert target_year < reference_year
    previous_year = reference_year - 1

    filiation_data_reference_to_previous_year = get_filiation_data(
        echelon_territorial, reference_year, previous_year
    )

    if echelon_territorial == EchelonTerritorial.COMMUNE:
        colonne_code_prefix = f"Code INSEE de la commune"
    elif echelon_territorial == EchelonTerritorial.EPCI:
        colonne_code_prefix = "Numéro SIREN"
    else:
        assert echelon_territorial == EchelonTerritorial.DEPARTEMENT
        colonne_code_prefix = ""

    colonne_code = f"{colonne_code_prefix} {reference_year}"
    colonne_code_previous_year = f"{colonne_code_prefix} {previous_year}"

    filter_territory = (
        filiation_data_reference_to_previous_year[colonne_code] == territory_code
    )

    territory_data = filiation_data_reference_to_previous_year[filter_territory]
    results_count = len(territory_data.index)

    if results_count == 0:
        # the territory hasn't changed ; it was not mentioned in filiation CSV
        previous_year_filiation = [territory_code]
    else:
        previous_year_filiation = territory_data[colonne_code_previous_year].tolist()

    if target_year == previous_year:
        logger.debug(
            f"Année cible atteinte. En {target_year}, les ancètres de {territory_code} ({echelon_territorial.value} {reference_year}) sont : {previous_year_filiation}"
        )
        return previous_year_filiation
    else:
        logger.debug(
            f"Année cible non atteinte. Recherche des ancêtres de l'année {previous_year}..."
        )
        recursive_call_result = []
        recursive_call_result += [
            # join recursive calls to flatten the list of lists
            get_territory_past_filiation(
                echelon_territorial, code, previous_year, target_year
            )
            for code in previous_year_filiation
        ]
        recursive_call_result = [
            item for sublist in recursive_call_result for item in sublist
        ]
        return recursive_call_result


def precision_simulation(dotations_criteres, adapted_data):
    simulated_data = calculate_dotations(adapted_data, dotations_criteres)
    dotations = [
        "dotation_forfaitaire",
        "dsu_montant",
        "dsr_fraction_bourg_centre",
        "dsr_fraction_perequation",
        "dsr_fraction_cible",
    ]
    print("CALCUL DES ECARTS")
    print("#################")

    for dotation in dotations:
        simulated_data[dotation].fillna(0)
        dotations_criteres[dotation].fillna(0)
        dotations_criteres[dotation + "_simu"] = simulated_data[dotation]
        dotations_criteres[dotation + "_diff"] = (
            dotations_criteres[dotation + "_simu"] - dotations_criteres[dotation]
        )
        dotations_criteres[dotation + "_ecart"] = (
            dotations_criteres[dotation + "_diff"] / dotations_criteres[dotation]
        ) * 100

        dotations_criteres[dotation + "_ecart"] = dotations_criteres[
            dotation + "_ecart"
        ].replace([np.inf, -np.inf], np.nan)

        moyenne_ecart = str(
            dotations_criteres[~dotations_criteres[dotation + "_ecart"].isnull()][
                dotation + "_ecart"
            ].mean()
        )
        print("#################")
        print("ECART MOYEN POUR " + dotation + " EST DE : " + moyenne_ecart)
        print("#################")
    # dotations_criteres.to_csv("simulation.csv")

    dotations_criteres_abs = dotations_criteres.copy()

    for dotation in dotations:
        dotations_criteres_abs[dotation + "_ecart"] = dotations_criteres_abs[
            dotation + "_ecart"
        ].abs()
        dotations_criteres_abs[dotation + "_diff"] = dotations_criteres_abs[
            dotation + "_diff"
        ].abs()

    dotations_criteres_abs["max_ecart"] = dotations_criteres_abs[
        [
            "dotation_forfaitaire_ecart",
            "dsu_montant_ecart",
            "dsr_fraction_bourg_centre_ecart",
            "dsr_fraction_perequation_ecart",
            "dsr_fraction_cible_ecart",
        ]
    ].max(axis=1)
    dotations_criteres_abs["max_diff"] = dotations_criteres_abs[
        [
            "dotation_forfaitaire_diff",
            "dsu_montant_diff",
            "dsr_fraction_bourg_centre_diff",
            "dsr_fraction_perequation_diff",
            "dsr_fraction_cible_diff",
        ]
    ].max(axis=1)

    return dotations_criteres_abs[
        (dotations_criteres_abs["max_diff"] >= 5000)
        | (dotations_criteres_abs["max_ecart"] >= 10)
    ]["code"].to_list()
