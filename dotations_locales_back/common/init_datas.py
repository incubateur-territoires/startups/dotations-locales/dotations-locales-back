from os import getcwd

from dotations_locales_back.common.load_dgcl_dotations_data import (
    load_dotation_data,
    concat_dotation_dfs,
)
from dotations_locales_back.common.load_dgcl_criteres_data import (
    load_commune_dgcl_data,
    load_commune_dbio_data,
    load_epci_dgcl_data,
    load_dpt_dgcl_data,
    dsr_calculation,
)
from dotations_locales_back.common.utils import precision_simulation
from dotations_locales_back.common import DATA_DIRECTORY


from dotations_locales_back.common.mapping.commune.criteres_dgcl_2020 import (
    filename_commune_criteres_2020,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2021 import (
    filename_commune_criteres_2021,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2022 import (
    filename_commune_criteres_2022,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2023 import (
    filename_commune_criteres_2023,
)
from dotations_locales_back.common.mapping.commune.dotation_elu_local import (
    filename_commune_dpel_2020,
    filename_commune_dpel_2021,
    filename_commune_dpel_2022,
    filename_commune_dpel_2023,
)
from dotations_locales_back.common.mapping.commune.dotation_biodiversite import (
    filename_commune_dbio_2021,
    filename_commune_dbio_2022,
    filename_commune_dbio_2023,
)

from dotations_locales_back.common.mapping.epci.criteres_dgcl_2023 import (
    filename_epci_criteres_2023,
)
from dotations_locales_back.common.mapping.epci.criteres_dgcl_2022 import (
    filename_epci_criteres_2022,
)
from dotations_locales_back.common.mapping.epci.criteres_dgcl_2021 import (
    filename_epci_criteres_2021,
)

from dotations_locales_back.common.mapping.departement.criteres_dgcl_2023 import (
    filename_departement_criteres_2023,
)
from dotations_locales_back.common.mapping.departement.criteres_dgcl_2022 import (
    filename_departement_criteres_2022,
)
from dotations_locales_back.common.mapping.departement.criteres_dgcl_2021 import (
    filename_departement_criteres_2021,
)

from dotations_locales_back.simulation.simulation import fully_adapted_data_2022


# DGCL criteres data
# COMMUNE
# CRITERES DOTATIONS FAISANT PARTIE DE LA DGF
COMMUNE_CRITERES_2020_PATH = DATA_DIRECTORY + filename_commune_criteres_2020
COMMUNE_CRITERES_2021_PATH = DATA_DIRECTORY + filename_commune_criteres_2021
COMMUNE_CRITERES_2022_PATH = DATA_DIRECTORY + filename_commune_criteres_2022
COMMUNE_CRITERES_2023_PATH = DATA_DIRECTORY + filename_commune_criteres_2023

# CRITERES DPEL
DPEL_2020_PATH = DATA_DIRECTORY + filename_commune_dpel_2020
DPEL_2021_PATH = DATA_DIRECTORY + filename_commune_dpel_2021
DPEL_2022_PATH = DATA_DIRECTORY + filename_commune_dpel_2022
DPEL_2023_PATH = DATA_DIRECTORY + filename_commune_dpel_2023

# CRITERES DOTATION BIODIVERSITE
DOTATION_BIODIVERSITE_2021_PATH = DATA_DIRECTORY + filename_commune_dbio_2021
DOTATION_BIODIVERSITE_2022_PATH = DATA_DIRECTORY + filename_commune_dbio_2022
DOTATION_BIODIVERSITE_2023_PATH = DATA_DIRECTORY + filename_commune_dbio_2023

# EPCI
# CRITERES DOTATIONS EPCI
EPCI_CRITERES_2021_PATH = DATA_DIRECTORY + filename_epci_criteres_2021
EPCI_CRITERES_2022_PATH = DATA_DIRECTORY + filename_epci_criteres_2022
EPCI_CRITERES_2023_PATH = DATA_DIRECTORY + filename_epci_criteres_2023

# DEPARTEMENT
# CRITERES DOTATIONS DEPARTEMENT
DEPARTEMENT_CRITERES_2021_PATH = DATA_DIRECTORY + filename_departement_criteres_2021
DEPARTEMENT_CRITERES_2022_PATH = DATA_DIRECTORY + filename_departement_criteres_2022
DEPARTEMENT_CRITERES_2023_PATH = DATA_DIRECTORY + filename_departement_criteres_2023


def init_dotations_data():
    """Initialise les données des dotations et des critères.
    Retourne un dictionnaire contenant les dataframes contenant les données de critères et de dotations par année
    Retourne également une liste de communes dont la précision de la simulation pour 2022 n'est pas satisfaisante
    """
    # COMMUNES

    # On charge les données des critères de repartiion des dotations DGF pour les années précédentes
    commune_dotations_criteres_2020 = load_commune_dgcl_data(
        COMMUNE_CRITERES_2020_PATH, 2020
    )
    commune_dotations_criteres_2020["dotation_amorcage"] = 0
    commune_dotations_criteres_2021 = load_commune_dgcl_data(
        COMMUNE_CRITERES_2021_PATH, 2021
    )
    commune_dotations_criteres_2022 = load_commune_dgcl_data(
        COMMUNE_CRITERES_2022_PATH, 2022
    )
    commune_dotations_criteres_2023 = load_commune_dgcl_data(
        COMMUNE_CRITERES_2023_PATH, 2023
    )

    # On charge les données des montants de la dotation élu local
    commune_dotation_elu_local_2020 = load_dotation_data(
        DPEL_2020_PATH, "dotation_elu_local"
    )
    commune_dotation_elu_local_2021 = load_dotation_data(
        DPEL_2021_PATH, "dotation_elu_local"
    )
    commune_dotation_elu_local_2022 = load_dotation_data(
        DPEL_2022_PATH, "dotation_elu_local"
    )
    commune_dotation_elu_local_2023 = load_dotation_data(
        DPEL_2023_PATH, "dotation_elu_local"
    )

    commune_dotation_biodiversite_2021 = load_commune_dbio_data(
        DOTATION_BIODIVERSITE_2021_PATH, 2021
    )
    commune_dotation_biodiversite_2022 = load_commune_dbio_data(
        DOTATION_BIODIVERSITE_2022_PATH, 2022
    )
    commune_dotation_biodiversite_2023 = load_dotation_data(
        DOTATION_BIODIVERSITE_2023_PATH, "dotation_biodiversite"
    )

    # On merge les données de DPEL et DBIO avec le reste
    commune_dotations_criteres_2020 = commune_dotations_criteres_2020.merge(
        commune_dotation_elu_local_2020,
        how="left",
        suffixes=(None, "_y"),
        on="code",
    )
    # N'ayant pas les données pour 2020 pour la Dbio, on met à 0
    commune_dotations_criteres_2020["dotation_biodiversite"] = 0
    commune_dotations_criteres_2021 = commune_dotations_criteres_2021.merge(
        commune_dotation_elu_local_2021,
        how="left",
        suffixes=(None, "_y"),
        on="code",
    )
    commune_dotations_criteres_2021 = commune_dotations_criteres_2021.merge(
        commune_dotation_biodiversite_2021, how="left", on="code"
    )
    commune_dotations_criteres_2021[
        "dotation_biodiversite"
    ] = commune_dotations_criteres_2021["dotation_biodiversite"].fillna(0)
    commune_dotations_criteres_2022 = commune_dotations_criteres_2022.merge(
        commune_dotation_elu_local_2022,
        how="left",
        suffixes=(None, "_y"),
        on="code",
    )
    commune_dotations_criteres_2022 = commune_dotations_criteres_2022.merge(
        commune_dotation_biodiversite_2022, how="left", on="code"
    )
    commune_dotations_criteres_2022[
        "dotation_biodiversite"
    ] = commune_dotations_criteres_2022["dotation_biodiversite"].fillna(0)

    commune_dotations_criteres_2023 = commune_dotations_criteres_2023.merge(
        commune_dotation_elu_local_2023,
        how="left",
        suffixes=(None, "_y"),
        on="code",
    )
    commune_dotations_criteres_2023 = commune_dotations_criteres_2023.merge(
        commune_dotation_biodiversite_2023, how="left", suffixes=(None, "_y"), on="code"
    )
    commune_dotations_criteres_2023[
        "dotation_biodiversite"
    ] = commune_dotations_criteres_2023["dotation_biodiversite"].fillna(0)

    liste_commune_avertissement_2022 = precision_simulation(
        commune_dotations_criteres_2022, fully_adapted_data_2022
    )

    commune_dotations_criteres = {
        "2023": commune_dotations_criteres_2023,
        "2022": commune_dotations_criteres_2022,
        "2021": commune_dotations_criteres_2021,
        "2020": commune_dotations_criteres_2020,
    }

    # EPCI
    # On charge les données des critères de repartiion des dotations DGF EPCI
    epci_dotations_criteres_2021 = load_epci_dgcl_data(EPCI_CRITERES_2021_PATH, 2021)
    epci_dotations_criteres_2022 = load_epci_dgcl_data(EPCI_CRITERES_2022_PATH, 2022)
    epci_dotations_criteres_2023 = load_epci_dgcl_data(EPCI_CRITERES_2023_PATH, 2023)

    epci_dotations_criteres = {
        "2023": epci_dotations_criteres_2023,
        "2022": epci_dotations_criteres_2022,
        "2021": epci_dotations_criteres_2021,
    }

    # DEPARTMENTS

    departement_dotations_criteres_2021 = load_dpt_dgcl_data(
        DEPARTEMENT_CRITERES_2021_PATH, 2021
    )
    departement_dotations_criteres_2022 = load_dpt_dgcl_data(
        DEPARTEMENT_CRITERES_2022_PATH, 2022
    )
    departement_dotations_criteres_2023 = load_dpt_dgcl_data(
        DEPARTEMENT_CRITERES_2023_PATH, 2023
    )

    departement_dotations_criteres = {
        "2023": departement_dotations_criteres_2023,
        "2022": departement_dotations_criteres_2022,
        "2021": departement_dotations_criteres_2021,
    }

    return (
        commune_dotations_criteres,
        liste_commune_avertissement_2022,
        epci_dotations_criteres,
        departement_dotations_criteres,
    )
