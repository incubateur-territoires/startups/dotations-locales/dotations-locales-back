import logging
import os
from pandas import read_csv

from dotations_locales_back.common import EchelonTerritorial, DATA_DIRECTORY


logger = logging.getLogger(__name__)
logger.level = logging.DEBUG


def load_filiation_data(path, columns, columns_dtypes):
    """Renvoie un dataframe qui contient les données d'un fichier de filiation.
    Les données sont mises en forme pour faciliter leur utilisation dans le code.
    Paramètres :
        - path : chemin du fichier csv contenant la filiation d'un territoire sur une année donnée
        - columns : la liste des noms de colonnes du fichier de filiation
        - columns_dtypes : le dictionnaire des types de chacune des colonnes du fichier de filiation
    """

    try:
        filiation_data = read_csv(
            path,
            delimiter=";",
            header=0,
            names=columns,
            dtype=columns_dtypes,
        )
        logger.debug(f"Fichier de filiation {path} trouvé et lu.")
        # replace all NaN by an empty string
        return filiation_data.fillna("")

    except FileNotFoundError:
        logger.error(f"Fichier de filiation introuvable : {path}")
        logger.error(f"Contenu du répertoire recherché : {os.listdir('.')}")
        logger.error(f"Chemin de fichier recherché : {os.getcwd()}")
        raise


def get_filiation_data(echelon_territorial, reference_year, previous_year):
    filiation_data_filename = (
        f"filiation_{echelon_territorial.value}_{previous_year}-{reference_year}.csv"
    )
    filiation_data_path = DATA_DIRECTORY + filiation_data_filename

    if echelon_territorial == EchelonTerritorial.COMMUNE:
        columns = [
            f"Code INSEE de la commune {previous_year}",
            f"Nom de la commune {previous_year}",
            f"Code INSEE de la commune {reference_year}",
            f"Nom de la commune {reference_year}",
            "Date de l'arrêté préfectoral",
        ]
        columns_dtypes = {
            f"Code INSEE de la commune {previous_year}": str,
            f"Nom de la commune {previous_year}": str,
            f"Code INSEE de la commune {reference_year}": str,
            f"Nom de la commune {reference_year}": str,
            "Date de l'arrêté préfectoral": str,
        }
    elif echelon_territorial == EchelonTerritorial.EPCI:
        columns = [
            f"Numéro SIREN {previous_year}",
            f"Nom EPCI {previous_year}",
            f"Numéro SIREN {reference_year}",
            f"Nom EPCI {reference_year}",
        ]
        columns_dtypes = {
            f"Numéro SIREN {previous_year}": str,
            f"Nom EPCI {previous_year}": str,
            f"Numéro SIREN {reference_year}": str,
            f"Nom EPCI {reference_year}": str,
        }
    else:
        # TODO départements
        assert echelon_territorial == EchelonTerritorial.DEPARTEMENT

        # 01/01/2021 - Naissance de la Collectivité européenne d'Alsace (CEA)
        # https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042472268
        columns = []
        columns_dtypes = {}

    filiation_data_reference_to_previous_year = load_filiation_data(
        filiation_data_path, columns, columns_dtypes
    )

    return filiation_data_reference_to_previous_year
