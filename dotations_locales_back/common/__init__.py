from enum import Enum
from os import getcwd

DATA_DIRECTORY = getcwd() + "/data/"


class EchelonTerritorial(Enum):
    COMMUNE = "commune"
    EPCI = "epci"
    DEPARTEMENT = "departement"
