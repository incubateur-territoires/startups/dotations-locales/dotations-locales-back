import pandas
from pathlib import Path
from pandas.api.types import is_string_dtype
from os import listdir, getcwd

from dotations_locales_back.common import EchelonTerritorial

from dotations_locales_back.common.mapping.commune.criteres_dgcl_2020 import (
    commune_columns_to_keep_2020,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2021 import (
    commune_columns_to_keep_2021,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2022 import (
    commune_columns_to_keep_2022,
)
from dotations_locales_back.common.mapping.commune.criteres_dgcl_2023 import (
    commune_columns_to_keep_2023,
)
from dotations_locales_back.common.mapping.commune.dotation_biodiversite import (
    columns_to_keep_commune_dbio_2023,
    columns_to_keep_commune_dbio_2022,
    columns_to_keep_commune_dbio_2021,
)

from dotations_locales_back.common.mapping.epci.criteres_dgcl_2021 import (
    epci_columns_to_keep_2021,
)
from dotations_locales_back.common.mapping.epci.criteres_dgcl_2022 import (
    epci_columns_to_keep_2022,
)
from dotations_locales_back.common.mapping.epci.criteres_dgcl_2023 import (
    epci_columns_to_keep_2023,
)

from dotations_locales_back.common.mapping.departement.criteres_dgcl_2021 import (
    departement_columns_to_keep_2021,
)
from dotations_locales_back.common.mapping.departement.criteres_dgcl_2022 import (
    departement_columns_to_keep_2022,
)
from dotations_locales_back.common.mapping.departement.criteres_dgcl_2023 import (
    departement_columns_to_keep_2023,
)


"""Ensemble de fonctions permettant de mettre en forme les données contenant les critères de repartition des dotations.
Disponible ici : http://www.dotations-dgcl.interieur.gouv.fr/consultation/criteres_repartition.php
Les fichiers sont téléchargés puis mis au format csv et se trouvent dans le dossier /data du projet. 
"""

code_insee = "Informations générales - Code INSEE de la commune"

BOOL_COLUMNS = [
    "zone_de_montagne",
    "insulaire",
    "zrr",
    "bureau_centralisateur",
    "chef_lieu_arrondissement",
    "chef_lieu_departement_dans_agglomeration",
]

COMMUNE_DGF_DOTATIONS_LIST = [
    "dotation_forfaitaire",
    "dsu_montant",
    "dotation_nationale_perequation",
    "dotation_solidarite_rurale",
    "dotation_amenagement_communes_outre_mer",
]

EPCI_DGF_DOTATIONS_LIST = [
    "dotation_compensation",
    "dotation_groupements_touristiques",
    "dotation_intercommunalite",
]

DPT_DGF_DOTATIONS_LIST = [
    "dotation_compensation",
    "dotation_forfaitaire",
    "dotation_perequation_urbaine",
    "dotation_fonctionnement_minimale",
]


def load_dgcl_file(path="/data/criteres_repartition_2021.csv"):
    try:
        print(f"Loading {Path(path).resolve()}")
        data = pandas.read_csv(path, decimal=",", dtype={code_insee: str})
    except FileNotFoundError:
        print("file", path, "was not found")
        print("ls :", listdir("."))
        print("cwd :", getcwd())
        raise
    return data


def delete_unecessary_columns(data, columns_to_keep):
    """Supprime les colonnes inutiles de la dataframe pour l'API du MVP"""
    data = data[list(columns_to_keep.keys())]
    return data


def rename_columns(data, columns_to_rename):
    """Renome les colonnes de data avec les noms de variables openfisca
    La table de renommage est fixée dans ce fichier python
    Arguments :
    - data : dataframe contenant les données de la DGCL
    """

    data.rename(
        columns={
            old_name: new_name for old_name, new_name in columns_to_rename.items()
        },
        inplace=True,
    )
    return data


def dsr_calculation(data):
    """Permet de calculer la dotation de solidarité rurale pour chaque commune.
    Arguments :
    - data : dataframe contenant les données de la DGCL avec colonnes nommées avec les noms de variables openfisca
    Retourne :
    - data : la même dataframe avec une colonne en plus contenant le montant de la DSR
    """
    data["dotation_solidarite_rurale"] = (
        data["dsr_fraction_bourg_centre"]
        + data["dsr_fraction_perequation"]
        + data["dsr_fraction_cible"]
    )
    return data


def rrf_parts_calculation(echelon_territorial, data, dotations_list):
    """Permet de calculer la part des dotations dans les recettes réelles de fonctionnement (RRF).
    - data : dataframe contenant les données de la DGCL avec colonnes nommées avec les noms de variables openfisca
    Retourne :
    - data : la même dataframe avec une colonne en plus contenant la part des dotations dans les recettes réelles de fonctionnement
    """
    data["dotation_globale_fonctionnement"] = data[dotations_list].sum(axis=1)
    if echelon_territorial == EchelonTerritorial.EPCI:
        data["recettes_reelles_fonctionnement"] = data["revenu_epci"].astype(float)
    else:
        data["recettes_reelles_fonctionnement"] = data[
            "recettes_reelles_fonctionnement"
        ].astype(float)

    data["part_dotation_rrf"] = (
        (
            data["dotation_globale_fonctionnement"]
            / data["recettes_reelles_fonctionnement"]
        )
        * 100
    ).round(1)
    return data


def convert_cols_to_real_bool(data, bool_col_list):
    """Convertit les colonnes contenant des "OUI" ou "NON" en vrai booléens "True" ou "False"
    La liste des colonnes est fixée "en dur" dans la fonction
    Arguments :
    - data : dataframe contenant les données de la DGCL avec colonnes nommées avec les noms de variables openfisca
    Retourne :
    - data : la même dataframe avec des colonnes contenant des vrais booléens"""

    for col in bool_col_list:
        if is_string_dtype(data[col]):

            # Les colonnes qui contiennent des "OUI" "NON"
            if "OUI" in data[col].values:
                data[col] = data[col].str.contains(pat="oui", case=False)

            # Les colonnes qui contiennent soit 1 soit le code commune lorsque vrai
            else:
                data[col] = data[col].replace("nd", "0").copy()
                data[col].replace(to_replace=r"^\d{5}$", value="1", regex=True)
                data[col] = data[col].astype(bool)

        # Les colonnes qui contiennent soit 0 ou 1 et de type int
        else:
            data[col] = data[col].astype(bool)
    return data


def convert_cols_types(data):
    """Convertit le types de certaines colonnes contenant des données aux types mixtes.
    On remplace aussi les valeurs "nd" qui, à priori signifie "Non défini", par la valeur 0.
    La liste des colonnes est fixée "en dur" dans la fonction
    Arguments :
    - data : dataframe contenant les données de la DGCL avec colonnes nommées avec les noms de variables openfisca
    Retourne :
    - data : la même dataframe avec les types des colonnes convertis"""
    columns_list_with_nd = [
        "population_enfants",
        "longueur_voirie",
        "part_population_canton",
        "population_dgf_agglomeration",
        "population_dgf_departement_agglomeration",
        "nombre_beneficiaires_aides_au_logement",
        "revenu_total",
        "dsr_fraction_bourg_centre",
        "dsr_fraction_perequation",
        "dsr_fraction_cible",
    ]

    columns_list_to_int = [
        "population_enfants",
        "population_dgf_agglomeration",
        "population_dgf_departement_agglomeration",
        "nombre_beneficiaires_aides_au_logement",
    ]

    columns_list_to_float = [
        "longueur_voirie",
        "population_dgf_majoree",
        "part_population_canton",
        "potentiel_financier_par_habitant",
        "effort_fiscal",
        "revenu_total",
        "superficie",
        "recettes_reelles_fonctionnement",
        "dsr_fraction_bourg_centre",
        "dsr_fraction_perequation",
        "dsr_fraction_cible",
    ]

    for col in columns_list_with_nd:
        data[col] = data[col].replace("nd", 0)
        data[col] = data[col].replace("ND", 0)

    for col in columns_list_to_int:
        data[col] = data[col].astype(int)

    for col in columns_list_to_float:
        data[col] = data[col].astype(float)

    return data


def add_list_column(data, groupby_cols, list_col, new_col_name):
    """
    Group a dataframe by one or more columns and create a new column with a list of values from another column.

    Args:
        data (pandas.DataFrame): The dataframe to group.
        groupby_cols (list): A list of column names to group by.
        list_col (str): The name of the column to create a list of values from.
        new_col_name (str): The name of the new column to create.

    Returns:
        pandas.DataFrame: The grouped dataframe with a new column containing a list of values.
    """
    # Group the DataFrame by the specified columns and create a new column with a list of values
    group_dict = data.groupby(groupby_cols)[list_col].apply(list).to_dict()
    data[new_col_name] = data.set_index(groupby_cols).index.map(group_dict)
    return data


def extend_dpt_commune_list(data, col_name):
    """
    This function extends the list of communes in a department to include the communes with the strates equal to its strate +/- 1.
    Args:
        data (pandas.DataFrame): The dataframe to check.
        col_name (str): The name of the column to check.
    """
    if data[col_name].apply(len).min() < 20:
        less_than_20 = data[data[col_name].apply(len) < 20]

        def find_neighboring_cities(row):
            strate = row["strate_demographique"]
            code_dpt = row["code_dpt"]
            lower_strate = strate - 1
            upper_strate = strate + 1
            neighboring_cities = less_than_20[
                (
                    less_than_20["strate_demographique"].isin(
                        [lower_strate, upper_strate]
                    )
                )
                & (less_than_20["code_dpt"] == code_dpt)
            ]["code"].tolist()
            return neighboring_cities

        # Apply the function to each row and create a new column 'neighboring_cities'
        less_than_20["neighboring_cities"] = less_than_20.apply(
            find_neighboring_cities, axis=1
        )
        data["dpt_commune_list"] = (
            data["dpt_commune_list"] + less_than_20["neighboring_cities"]
        ).fillna(data["dpt_commune_list"])
    return data


def load_commune_dgcl_data(path, year):
    data = load_dgcl_file(path)
    if year == 2023:
        data = delete_unecessary_columns(data, commune_columns_to_keep_2023)
        data = rename_columns(data, commune_columns_to_keep_2023)
        data = convert_cols_types(data)
        # We add the list of communes in the department and with the same strate to each commune
        data = add_list_column(
            data, ["code_dpt", "strate_demographique"], "code", "dpt_commune_list"
        )
        # We extend the list of communes in the department to include the communes with the strates equal to its strate +/- 1
        data = extend_dpt_commune_list(data, "dpt_commune_list")
        # We add the list of communes in the epci to each commune
        data = add_list_column(data, ["code_epci"], "code", "epci_commune_list")
    elif year == 2022:
        data = delete_unecessary_columns(data, commune_columns_to_keep_2022)
        data = rename_columns(data, commune_columns_to_keep_2022)
        data = convert_cols_types(data)
    elif year == 2021:
        data = delete_unecessary_columns(data, commune_columns_to_keep_2021)
        data = rename_columns(data, commune_columns_to_keep_2021)
    elif year == 2020:
        data = delete_unecessary_columns(data, commune_columns_to_keep_2020)
        data = rename_columns(data, commune_columns_to_keep_2020)
    else:
        raise ValueError(
            "Year must be 2020, 2021 or 2022, others years are not supported"
        )
    data = dsr_calculation(data)
    data = rrf_parts_calculation(
        EchelonTerritorial.COMMUNE, data, COMMUNE_DGF_DOTATIONS_LIST
    )
    data = convert_cols_to_real_bool(data, BOOL_COLUMNS)
    return data


def load_commune_dbio_data(path, year):
    data = load_dgcl_file(path)
    if year == 2023:
        data = delete_unecessary_columns(data, columns_to_keep_commune_dbio_2023)
        data = rename_columns(data, columns_to_keep_commune_dbio_2023)
    if year == 2022:
        data = delete_unecessary_columns(data, columns_to_keep_commune_dbio_2022)
        data = rename_columns(data, columns_to_keep_commune_dbio_2022)
    elif year == 2021:
        data = delete_unecessary_columns(data, columns_to_keep_commune_dbio_2021)
        data = rename_columns(data, columns_to_keep_commune_dbio_2021)
    return data


def load_epci_dgcl_data(path, year):
    data = load_dgcl_file(path)
    if year == 2023:
        data = delete_unecessary_columns(data, epci_columns_to_keep_2023)
        data = rename_columns(data, epci_columns_to_keep_2023)
        data["code"] = data["code"].astype(str)
        data = add_list_column(data, ["departement"], "code", "dpt_epci_list")
    elif year == 2022:
        data = delete_unecessary_columns(data, epci_columns_to_keep_2022)
        data = rename_columns(data, epci_columns_to_keep_2022)
    elif year == 2021:
        data = delete_unecessary_columns(data, epci_columns_to_keep_2021)
        data = rename_columns(data, epci_columns_to_keep_2021)
    else:
        raise ValueError("Year must be 2021 or 2022, others years are not supported")
    data["code"] = data["code"].astype(str)
    data = rrf_parts_calculation(EchelonTerritorial.EPCI, data, EPCI_DGF_DOTATIONS_LIST)
    return data


def load_dpt_dgcl_data(path, year):
    data = load_dgcl_file(path)
    if year == 2023:
        data = delete_unecessary_columns(data, departement_columns_to_keep_2023)
        data = rename_columns(data, departement_columns_to_keep_2023)
        data = add_list_column(data, ["code_region"], "code", "region_dpt_list")
    elif year == 2022:
        data = delete_unecessary_columns(data, departement_columns_to_keep_2022)
        data = rename_columns(data, departement_columns_to_keep_2022)
    elif year == 2021:
        data = delete_unecessary_columns(data, departement_columns_to_keep_2021)
        data = rename_columns(data, departement_columns_to_keep_2021)
    else:
        raise ValueError("Year must be 2021 or 2022, others years are not supported")
    data["code"] = data["code"].astype(str)
    data = rrf_parts_calculation(
        EchelonTerritorial.DEPARTEMENT, data, DPT_DGF_DOTATIONS_LIST
    )
    return data
